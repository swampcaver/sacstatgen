﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbArmor = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbSize = New System.Windows.Forms.ComboBox()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTroopName = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbRace = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuNewTroopList = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuOpenTroopList = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuSaveCurrent = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuLoadFromTroopList = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuLoadFromGlobalTroopList = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuRecalculateAllInTroopList = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckForUpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuPrintSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuPrintCurrentTroop = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuPrintArmyList = New System.Windows.Forms.ToolStripMenuItem()
        Me.menuPrintGlobalTroopList = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.SortByNameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SortByPointValueToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbTemplate = New System.Windows.Forms.ComboBox()
        Me.cbMeleeWpnsPri = New System.Windows.Forms.ComboBox()
        Me.cbRangedWeapons = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cbMeleeWpnsOff = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtMeleePriDesc = New System.Windows.Forms.TextBox()
        Me.txtMeleeOffDesc = New System.Windows.Forms.TextBox()
        Me.txtRangedDesc = New System.Windows.Forms.TextBox()
        Me.txtAttacks = New System.Windows.Forms.TextBox()
        Me.chkShield = New System.Windows.Forms.CheckBox()
        Me.lblOffHandWpns = New System.Windows.Forms.Label()
        Me.radOffHandBonus = New System.Windows.Forms.RadioButton()
        Me.radOffHandSec = New System.Windows.Forms.RadioButton()
        Me.radOffhandBoth = New System.Windows.Forms.RadioButton()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblPointValue = New System.Windows.Forms.Label()
        Me.txtTroopQualities = New System.Windows.Forms.TextBox()
        Me.btnQualities = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblMeleeDef = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblRangedDef = New System.Windows.Forms.Label()
        Me.chkArmorName = New System.Windows.Forms.CheckBox()
        Me.txtArmorName = New System.Windows.Forms.TextBox()
        Me.btnMoveDn = New System.Windows.Forms.Button()
        Me.txtMove = New System.Windows.Forms.TextBox()
        Me.btnMoveUp = New System.Windows.Forms.Button()
        Me.btnStrengthUp = New System.Windows.Forms.Button()
        Me.btnStrengthDn = New System.Windows.Forms.Button()
        Me.txtStrength = New System.Windows.Forms.TextBox()
        Me.btnMeleeSkillUp = New System.Windows.Forms.Button()
        Me.btnMeleeSkillDn = New System.Windows.Forms.Button()
        Me.txtMeleeSkill = New System.Windows.Forms.TextBox()
        Me.txtRangedSkill = New System.Windows.Forms.TextBox()
        Me.btnRangedSkillDn = New System.Windows.Forms.Button()
        Me.btnRangedSkillUp = New System.Windows.Forms.Button()
        Me.btnWoundsUp = New System.Windows.Forms.Button()
        Me.btnWoundsDn = New System.Windows.Forms.Button()
        Me.txtWounds = New System.Windows.Forms.TextBox()
        Me.btnCustomAttacks = New System.Windows.Forms.Button()
        Me.btnBasicAttacks = New System.Windows.Forms.Button()
        Me.chkSpellCaster = New System.Windows.Forms.CheckBox()
        Me.lblCasterLevel = New System.Windows.Forms.Label()
        Me.cbCasterLevel = New System.Windows.Forms.ComboBox()
        Me.lblSpellsKnown = New System.Windows.Forms.Label()
        Me.cbSpellSources = New System.Windows.Forms.ComboBox()
        Me.chkSingleSpell = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtFactions = New System.Windows.Forms.TextBox()
        Me.btnFactions = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblHealCost = New System.Windows.Forms.Label()
        Me.linkTroopCard = New System.Windows.Forms.LinkLabel()
        Me.lblToWound = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButtonNewTroopList = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonOpenTroopList = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonSaveCurrentTroop = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButtonLoadFromTroopList = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonLoadFromGlobalTroopList = New System.Windows.Forms.ToolStripButton()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(46, 209)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Strength"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(46, 235)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Melee Skill"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(46, 261)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Ranged Skill"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(46, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Move"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(272, 182)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Armor"
        '
        'cbArmor
        '
        Me.cbArmor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbArmor.FormattingEnabled = True
        Me.cbArmor.Items.AddRange(New Object() {"2 (None)", "3 (Leather)", "4 (Chain)", "5 (Scale)", "6 (Plate)"})
        Me.cbArmor.Location = New System.Drawing.Point(336, 181)
        Me.cbArmor.Name = "cbArmor"
        Me.cbArmor.Size = New System.Drawing.Size(92, 21)
        Me.cbArmor.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(272, 233)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Wounds"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(272, 261)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 15)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Size"
        '
        'cbSize
        '
        Me.cbSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSize.FormattingEnabled = True
        Me.cbSize.Items.AddRange(New Object() {"Small", "Medium", "Cavalry", "Large", "Huge"})
        Me.cbSize.Location = New System.Drawing.Point(336, 260)
        Me.cbSize.Name = "cbSize"
        Me.cbSize.Size = New System.Drawing.Size(92, 21)
        Me.cbSize.TabIndex = 6
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(459, 233)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(76, 39)
        Me.btnReset.TabIndex = 7
        Me.btnReset.Text = "Reset to Defaults"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(84, 60)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(76, 15)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Troop Name"
        '
        'txtTroopName
        '
        Me.txtTroopName.Location = New System.Drawing.Point(198, 60)
        Me.txtTroopName.Name = "txtTroopName"
        Me.txtTroopName.Size = New System.Drawing.Size(206, 20)
        Me.txtTroopName.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(32, 144)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(36, 15)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Race"
        '
        'cbRace
        '
        Me.cbRace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbRace.FormattingEnabled = True
        Me.cbRace.Items.AddRange(New Object() {"Human"})
        Me.cbRace.Location = New System.Drawing.Point(89, 144)
        Me.cbRace.Name = "cbRace"
        Me.cbRace.Size = New System.Drawing.Size(121, 21)
        Me.cbRace.Sorted = True
        Me.cbRace.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(39, 301)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(99, 15)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Special Qualities"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.CheckForUpdatesToolStripMenuItem, Me.menuPrintSave})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(611, 24)
        Me.MenuStrip1.TabIndex = 10
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuNewTroopList, Me.menuOpenTroopList, Me.menuSaveCurrent, Me.menuLoadFromTroopList, Me.menuLoadFromGlobalTroopList, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'menuNewTroopList
        '
        Me.menuNewTroopList.Name = "menuNewTroopList"
        Me.menuNewTroopList.Size = New System.Drawing.Size(220, 22)
        Me.menuNewTroopList.Text = "New Troop List"
        '
        'menuOpenTroopList
        '
        Me.menuOpenTroopList.Name = "menuOpenTroopList"
        Me.menuOpenTroopList.Size = New System.Drawing.Size(220, 22)
        Me.menuOpenTroopList.Text = "Open Troop List"
        '
        'menuSaveCurrent
        '
        Me.menuSaveCurrent.Enabled = False
        Me.menuSaveCurrent.Name = "menuSaveCurrent"
        Me.menuSaveCurrent.Size = New System.Drawing.Size(220, 22)
        Me.menuSaveCurrent.Text = "Save Current Troop"
        '
        'menuLoadFromTroopList
        '
        Me.menuLoadFromTroopList.Enabled = False
        Me.menuLoadFromTroopList.Name = "menuLoadFromTroopList"
        Me.menuLoadFromTroopList.Size = New System.Drawing.Size(220, 22)
        Me.menuLoadFromTroopList.Text = "Load From Troop List"
        '
        'menuLoadFromGlobalTroopList
        '
        Me.menuLoadFromGlobalTroopList.Name = "menuLoadFromGlobalTroopList"
        Me.menuLoadFromGlobalTroopList.Size = New System.Drawing.Size(220, 22)
        Me.menuLoadFromGlobalTroopList.Text = "Load from Global Troop List"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(220, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuRecalculateAllInTroopList})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'menuRecalculateAllInTroopList
        '
        Me.menuRecalculateAllInTroopList.Enabled = False
        Me.menuRecalculateAllInTroopList.Name = "menuRecalculateAllInTroopList"
        Me.menuRecalculateAllInTroopList.Size = New System.Drawing.Size(216, 22)
        Me.menuRecalculateAllInTroopList.Text = "Recalculate all in Troop List"
        '
        'CheckForUpdatesToolStripMenuItem
        '
        Me.CheckForUpdatesToolStripMenuItem.Name = "CheckForUpdatesToolStripMenuItem"
        Me.CheckForUpdatesToolStripMenuItem.Size = New System.Drawing.Size(118, 20)
        Me.CheckForUpdatesToolStripMenuItem.Text = "Check For Updates"
        '
        'menuPrintSave
        '
        Me.menuPrintSave.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuPrintCurrentTroop, Me.menuPrintArmyList, Me.menuPrintGlobalTroopList, Me.ToolStripSeparator1, Me.SortByNameToolStripMenuItem, Me.SortByPointValueToolStripMenuItem})
        Me.menuPrintSave.Name = "menuPrintSave"
        Me.menuPrintSave.Size = New System.Drawing.Size(83, 20)
        Me.menuPrintSave.Text = "Export/Print"
        Me.menuPrintSave.Visible = False
        '
        'menuPrintCurrentTroop
        '
        Me.menuPrintCurrentTroop.Name = "menuPrintCurrentTroop"
        Me.menuPrintCurrentTroop.Size = New System.Drawing.Size(173, 22)
        Me.menuPrintCurrentTroop.Text = "Current Troop"
        '
        'menuPrintArmyList
        '
        Me.menuPrintArmyList.Enabled = False
        Me.menuPrintArmyList.Name = "menuPrintArmyList"
        Me.menuPrintArmyList.Size = New System.Drawing.Size(173, 22)
        Me.menuPrintArmyList.Text = "Army List"
        '
        'menuPrintGlobalTroopList
        '
        Me.menuPrintGlobalTroopList.Name = "menuPrintGlobalTroopList"
        Me.menuPrintGlobalTroopList.Size = New System.Drawing.Size(173, 22)
        Me.menuPrintGlobalTroopList.Text = "Global Troop List"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(170, 6)
        '
        'SortByNameToolStripMenuItem
        '
        Me.SortByNameToolStripMenuItem.Checked = True
        Me.SortByNameToolStripMenuItem.CheckOnClick = True
        Me.SortByNameToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SortByNameToolStripMenuItem.Name = "SortByNameToolStripMenuItem"
        Me.SortByNameToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.SortByNameToolStripMenuItem.Text = "Sort by name"
        '
        'SortByPointValueToolStripMenuItem
        '
        Me.SortByPointValueToolStripMenuItem.CheckOnClick = True
        Me.SortByPointValueToolStripMenuItem.Name = "SortByPointValueToolStripMenuItem"
        Me.SortByPointValueToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.SortByPointValueToolStripMenuItem.Text = "Sort by point value"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(243, 146)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 15)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Template"
        '
        'cbTemplate
        '
        Me.cbTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTemplate.FormattingEnabled = True
        Me.cbTemplate.Location = New System.Drawing.Point(307, 143)
        Me.cbTemplate.Name = "cbTemplate"
        Me.cbTemplate.Size = New System.Drawing.Size(121, 21)
        Me.cbTemplate.Sorted = True
        Me.cbTemplate.TabIndex = 6
        '
        'cbMeleeWpnsPri
        '
        Me.cbMeleeWpnsPri.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbMeleeWpnsPri.FormattingEnabled = True
        Me.cbMeleeWpnsPri.Location = New System.Drawing.Point(39, 443)
        Me.cbMeleeWpnsPri.Name = "cbMeleeWpnsPri"
        Me.cbMeleeWpnsPri.Size = New System.Drawing.Size(121, 21)
        Me.cbMeleeWpnsPri.Sorted = True
        Me.cbMeleeWpnsPri.TabIndex = 11
        '
        'cbRangedWeapons
        '
        Me.cbRangedWeapons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbRangedWeapons.FormattingEnabled = True
        Me.cbRangedWeapons.Location = New System.Drawing.Point(39, 536)
        Me.cbRangedWeapons.Name = "cbRangedWeapons"
        Me.cbRangedWeapons.Size = New System.Drawing.Size(121, 21)
        Me.cbRangedWeapons.Sorted = True
        Me.cbRangedWeapons.TabIndex = 11
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(72, 427)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 13)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Primary"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(78, 520)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(45, 13)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Ranged"
        '
        'cbMeleeWpnsOff
        '
        Me.cbMeleeWpnsOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbMeleeWpnsOff.FormattingEnabled = True
        Me.cbMeleeWpnsOff.Location = New System.Drawing.Point(39, 489)
        Me.cbMeleeWpnsOff.Name = "cbMeleeWpnsOff"
        Me.cbMeleeWpnsOff.Size = New System.Drawing.Size(121, 21)
        Me.cbMeleeWpnsOff.Sorted = True
        Me.cbMeleeWpnsOff.TabIndex = 11
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(74, 473)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 13)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Off-Hand"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(36, 400)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 17)
        Me.Label15.TabIndex = 13
        Me.Label15.Text = "Weapons"
        '
        'txtMeleePriDesc
        '
        Me.txtMeleePriDesc.Location = New System.Drawing.Point(173, 443)
        Me.txtMeleePriDesc.Name = "txtMeleePriDesc"
        Me.txtMeleePriDesc.ReadOnly = True
        Me.txtMeleePriDesc.Size = New System.Drawing.Size(304, 20)
        Me.txtMeleePriDesc.TabIndex = 14
        '
        'txtMeleeOffDesc
        '
        Me.txtMeleeOffDesc.Location = New System.Drawing.Point(173, 490)
        Me.txtMeleeOffDesc.Name = "txtMeleeOffDesc"
        Me.txtMeleeOffDesc.ReadOnly = True
        Me.txtMeleeOffDesc.Size = New System.Drawing.Size(304, 20)
        Me.txtMeleeOffDesc.TabIndex = 14
        '
        'txtRangedDesc
        '
        Me.txtRangedDesc.Location = New System.Drawing.Point(173, 537)
        Me.txtRangedDesc.Name = "txtRangedDesc"
        Me.txtRangedDesc.ReadOnly = True
        Me.txtRangedDesc.Size = New System.Drawing.Size(304, 20)
        Me.txtRangedDesc.TabIndex = 14
        '
        'txtAttacks
        '
        Me.txtAttacks.Location = New System.Drawing.Point(49, 626)
        Me.txtAttacks.Multiline = True
        Me.txtAttacks.Name = "txtAttacks"
        Me.txtAttacks.ReadOnly = True
        Me.txtAttacks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAttacks.Size = New System.Drawing.Size(438, 174)
        Me.txtAttacks.TabIndex = 15
        '
        'chkShield
        '
        Me.chkShield.AutoSize = True
        Me.chkShield.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkShield.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkShield.Location = New System.Drawing.Point(270, 208)
        Me.chkShield.Name = "chkShield"
        Me.chkShield.Size = New System.Drawing.Size(79, 17)
        Me.chkShield.TabIndex = 16
        Me.chkShield.Text = "Shield?      "
        Me.chkShield.UseVisualStyleBackColor = True
        '
        'lblOffHandWpns
        '
        Me.lblOffHandWpns.AutoSize = True
        Me.lblOffHandWpns.Location = New System.Drawing.Point(71, 578)
        Me.lblOffHandWpns.Name = "lblOffHandWpns"
        Me.lblOffHandWpns.Size = New System.Drawing.Size(126, 13)
        Me.lblOffHandWpns.TabIndex = 17
        Me.lblOffHandWpns.Text = "Use off-hand weapon as:"
        '
        'radOffHandBonus
        '
        Me.radOffHandBonus.AutoSize = True
        Me.radOffHandBonus.Checked = True
        Me.radOffHandBonus.Location = New System.Drawing.Point(74, 594)
        Me.radOffHandBonus.Name = "radOffHandBonus"
        Me.radOffHandBonus.Size = New System.Drawing.Size(137, 17)
        Me.radOffHandBonus.TabIndex = 18
        Me.radOffHandBonus.TabStop = True
        Me.radOffHandBonus.Text = "Bonus to Primary attack"
        Me.radOffHandBonus.UseVisualStyleBackColor = True
        '
        'radOffHandSec
        '
        Me.radOffHandSec.AutoSize = True
        Me.radOffHandSec.Location = New System.Drawing.Point(217, 594)
        Me.radOffHandSec.Name = "radOffHandSec"
        Me.radOffHandSec.Size = New System.Drawing.Size(103, 17)
        Me.radOffHandSec.TabIndex = 18
        Me.radOffHandSec.Text = "A second attack"
        Me.radOffHandSec.UseVisualStyleBackColor = True
        '
        'radOffhandBoth
        '
        Me.radOffhandBoth.AutoSize = True
        Me.radOffhandBoth.Location = New System.Drawing.Point(326, 594)
        Me.radOffhandBoth.Name = "radOffhandBoth"
        Me.radOffhandBoth.Size = New System.Drawing.Size(47, 17)
        Me.radOffhandBoth.TabIndex = 18
        Me.radOffhandBoth.Text = "Both"
        Me.radOffhandBoth.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(517, 72)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 13)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "Point Value"
        '
        'lblPointValue
        '
        Me.lblPointValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPointValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPointValue.Location = New System.Drawing.Point(495, 85)
        Me.lblPointValue.Name = "lblPointValue"
        Me.lblPointValue.Size = New System.Drawing.Size(96, 39)
        Me.lblPointValue.TabIndex = 20
        Me.lblPointValue.Text = "0"
        Me.lblPointValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTroopQualities
        '
        Me.txtTroopQualities.Location = New System.Drawing.Point(138, 300)
        Me.txtTroopQualities.Multiline = True
        Me.txtTroopQualities.Name = "txtTroopQualities"
        Me.txtTroopQualities.ReadOnly = True
        Me.txtTroopQualities.Size = New System.Drawing.Size(342, 51)
        Me.txtTroopQualities.TabIndex = 22
        '
        'btnQualities
        '
        Me.btnQualities.Location = New System.Drawing.Point(49, 326)
        Me.btnQualities.Name = "btnQualities"
        Me.btnQualities.Size = New System.Drawing.Size(67, 25)
        Me.btnQualities.TabIndex = 23
        Me.btnQualities.Text = "Select"
        Me.btnQualities.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(505, 631)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(72, 35)
        Me.Label20.TabIndex = 24
        Me.Label20.Text = "Melee Defense"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMeleeDef
        '
        Me.lblMeleeDef.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMeleeDef.Location = New System.Drawing.Point(519, 660)
        Me.lblMeleeDef.Name = "lblMeleeDef"
        Me.lblMeleeDef.Size = New System.Drawing.Size(44, 28)
        Me.lblMeleeDef.TabIndex = 25
        Me.lblMeleeDef.Text = "0"
        Me.lblMeleeDef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(505, 697)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(72, 26)
        Me.Label22.TabIndex = 24
        Me.Label22.Text = "Ranged Defense"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRangedDef
        '
        Me.lblRangedDef.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRangedDef.Location = New System.Drawing.Point(518, 723)
        Me.lblRangedDef.Name = "lblRangedDef"
        Me.lblRangedDef.Size = New System.Drawing.Size(45, 23)
        Me.lblRangedDef.TabIndex = 25
        Me.lblRangedDef.Text = "0"
        Me.lblRangedDef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkArmorName
        '
        Me.chkArmorName.AutoSize = True
        Me.chkArmorName.Location = New System.Drawing.Point(434, 181)
        Me.chkArmorName.Name = "chkArmorName"
        Me.chkArmorName.Size = New System.Drawing.Size(122, 17)
        Me.chkArmorName.TabIndex = 26
        Me.chkArmorName.Text = "Custom Armor Name"
        Me.chkArmorName.UseVisualStyleBackColor = True
        '
        'txtArmorName
        '
        Me.txtArmorName.Location = New System.Drawing.Point(435, 201)
        Me.txtArmorName.Name = "txtArmorName"
        Me.txtArmorName.Size = New System.Drawing.Size(138, 20)
        Me.txtArmorName.TabIndex = 27
        Me.txtArmorName.Visible = False
        '
        'btnMoveDn
        '
        Me.btnMoveDn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoveDn.Location = New System.Drawing.Point(173, 180)
        Me.btnMoveDn.Name = "btnMoveDn"
        Me.btnMoveDn.Size = New System.Drawing.Size(25, 25)
        Me.btnMoveDn.TabIndex = 29
        Me.btnMoveDn.Text = "-"
        Me.btnMoveDn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMoveDn.UseVisualStyleBackColor = True
        '
        'txtMove
        '
        Me.txtMove.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMove.Location = New System.Drawing.Point(145, 180)
        Me.txtMove.Name = "txtMove"
        Me.txtMove.ReadOnly = True
        Me.txtMove.Size = New System.Drawing.Size(25, 23)
        Me.txtMove.TabIndex = 28
        Me.txtMove.TabStop = False
        '
        'btnMoveUp
        '
        Me.btnMoveUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoveUp.Location = New System.Drawing.Point(198, 180)
        Me.btnMoveUp.Name = "btnMoveUp"
        Me.btnMoveUp.Size = New System.Drawing.Size(25, 25)
        Me.btnMoveUp.TabIndex = 29
        Me.btnMoveUp.Text = "+"
        Me.btnMoveUp.UseVisualStyleBackColor = True
        '
        'btnStrengthUp
        '
        Me.btnStrengthUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStrengthUp.Location = New System.Drawing.Point(198, 206)
        Me.btnStrengthUp.Name = "btnStrengthUp"
        Me.btnStrengthUp.Size = New System.Drawing.Size(25, 25)
        Me.btnStrengthUp.TabIndex = 32
        Me.btnStrengthUp.Text = "+"
        Me.btnStrengthUp.UseVisualStyleBackColor = True
        '
        'btnStrengthDn
        '
        Me.btnStrengthDn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStrengthDn.Location = New System.Drawing.Point(173, 206)
        Me.btnStrengthDn.Name = "btnStrengthDn"
        Me.btnStrengthDn.Size = New System.Drawing.Size(25, 25)
        Me.btnStrengthDn.TabIndex = 31
        Me.btnStrengthDn.Text = "-"
        Me.btnStrengthDn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnStrengthDn.UseVisualStyleBackColor = True
        '
        'txtStrength
        '
        Me.txtStrength.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStrength.Location = New System.Drawing.Point(145, 206)
        Me.txtStrength.Name = "txtStrength"
        Me.txtStrength.ReadOnly = True
        Me.txtStrength.Size = New System.Drawing.Size(25, 23)
        Me.txtStrength.TabIndex = 30
        Me.txtStrength.TabStop = False
        '
        'btnMeleeSkillUp
        '
        Me.btnMeleeSkillUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMeleeSkillUp.Location = New System.Drawing.Point(198, 232)
        Me.btnMeleeSkillUp.Name = "btnMeleeSkillUp"
        Me.btnMeleeSkillUp.Size = New System.Drawing.Size(25, 25)
        Me.btnMeleeSkillUp.TabIndex = 35
        Me.btnMeleeSkillUp.Text = "+"
        Me.btnMeleeSkillUp.UseVisualStyleBackColor = True
        '
        'btnMeleeSkillDn
        '
        Me.btnMeleeSkillDn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMeleeSkillDn.Location = New System.Drawing.Point(173, 232)
        Me.btnMeleeSkillDn.Name = "btnMeleeSkillDn"
        Me.btnMeleeSkillDn.Size = New System.Drawing.Size(25, 25)
        Me.btnMeleeSkillDn.TabIndex = 34
        Me.btnMeleeSkillDn.Text = "-"
        Me.btnMeleeSkillDn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMeleeSkillDn.UseVisualStyleBackColor = True
        '
        'txtMeleeSkill
        '
        Me.txtMeleeSkill.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMeleeSkill.Location = New System.Drawing.Point(145, 232)
        Me.txtMeleeSkill.Name = "txtMeleeSkill"
        Me.txtMeleeSkill.ReadOnly = True
        Me.txtMeleeSkill.Size = New System.Drawing.Size(25, 23)
        Me.txtMeleeSkill.TabIndex = 33
        Me.txtMeleeSkill.TabStop = False
        '
        'txtRangedSkill
        '
        Me.txtRangedSkill.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRangedSkill.Location = New System.Drawing.Point(145, 258)
        Me.txtRangedSkill.Name = "txtRangedSkill"
        Me.txtRangedSkill.ReadOnly = True
        Me.txtRangedSkill.Size = New System.Drawing.Size(25, 23)
        Me.txtRangedSkill.TabIndex = 33
        Me.txtRangedSkill.TabStop = False
        '
        'btnRangedSkillDn
        '
        Me.btnRangedSkillDn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRangedSkillDn.Location = New System.Drawing.Point(173, 258)
        Me.btnRangedSkillDn.Name = "btnRangedSkillDn"
        Me.btnRangedSkillDn.Size = New System.Drawing.Size(25, 25)
        Me.btnRangedSkillDn.TabIndex = 34
        Me.btnRangedSkillDn.Text = "-"
        Me.btnRangedSkillDn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnRangedSkillDn.UseVisualStyleBackColor = True
        '
        'btnRangedSkillUp
        '
        Me.btnRangedSkillUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRangedSkillUp.Location = New System.Drawing.Point(198, 258)
        Me.btnRangedSkillUp.Name = "btnRangedSkillUp"
        Me.btnRangedSkillUp.Size = New System.Drawing.Size(25, 25)
        Me.btnRangedSkillUp.TabIndex = 35
        Me.btnRangedSkillUp.Text = "+"
        Me.btnRangedSkillUp.UseVisualStyleBackColor = True
        '
        'btnWoundsUp
        '
        Me.btnWoundsUp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWoundsUp.Location = New System.Drawing.Point(383, 229)
        Me.btnWoundsUp.Name = "btnWoundsUp"
        Me.btnWoundsUp.Size = New System.Drawing.Size(25, 25)
        Me.btnWoundsUp.TabIndex = 38
        Me.btnWoundsUp.Text = "+"
        Me.btnWoundsUp.UseVisualStyleBackColor = True
        '
        'btnWoundsDn
        '
        Me.btnWoundsDn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnWoundsDn.Location = New System.Drawing.Point(358, 229)
        Me.btnWoundsDn.Name = "btnWoundsDn"
        Me.btnWoundsDn.Size = New System.Drawing.Size(25, 25)
        Me.btnWoundsDn.TabIndex = 37
        Me.btnWoundsDn.Text = "-"
        Me.btnWoundsDn.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnWoundsDn.UseVisualStyleBackColor = True
        '
        'txtWounds
        '
        Me.txtWounds.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWounds.Location = New System.Drawing.Point(330, 229)
        Me.txtWounds.Name = "txtWounds"
        Me.txtWounds.ReadOnly = True
        Me.txtWounds.Size = New System.Drawing.Size(25, 23)
        Me.txtWounds.TabIndex = 36
        Me.txtWounds.TabStop = False
        '
        'btnCustomAttacks
        '
        Me.btnCustomAttacks.Location = New System.Drawing.Point(506, 444)
        Me.btnCustomAttacks.Name = "btnCustomAttacks"
        Me.btnCustomAttacks.Size = New System.Drawing.Size(73, 42)
        Me.btnCustomAttacks.TabIndex = 39
        Me.btnCustomAttacks.Text = "Advanced"
        Me.btnCustomAttacks.UseVisualStyleBackColor = True
        '
        'btnBasicAttacks
        '
        Me.btnBasicAttacks.Enabled = False
        Me.btnBasicAttacks.Location = New System.Drawing.Point(507, 502)
        Me.btnBasicAttacks.Name = "btnBasicAttacks"
        Me.btnBasicAttacks.Size = New System.Drawing.Size(73, 42)
        Me.btnBasicAttacks.TabIndex = 39
        Me.btnBasicAttacks.Text = "Revert to Basic"
        Me.btnBasicAttacks.UseVisualStyleBackColor = True
        '
        'chkSpellCaster
        '
        Me.chkSpellCaster.AutoSize = True
        Me.chkSpellCaster.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSpellCaster.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSpellCaster.Location = New System.Drawing.Point(42, 368)
        Me.chkSpellCaster.Name = "chkSpellCaster"
        Me.chkSpellCaster.Size = New System.Drawing.Size(92, 19)
        Me.chkSpellCaster.TabIndex = 41
        Me.chkSpellCaster.Text = "Spell Caster"
        Me.chkSpellCaster.UseVisualStyleBackColor = True
        '
        'lblCasterLevel
        '
        Me.lblCasterLevel.AutoSize = True
        Me.lblCasterLevel.Location = New System.Drawing.Point(154, 371)
        Me.lblCasterLevel.Name = "lblCasterLevel"
        Me.lblCasterLevel.Size = New System.Drawing.Size(66, 13)
        Me.lblCasterLevel.TabIndex = 42
        Me.lblCasterLevel.Text = "Caster Level"
        Me.lblCasterLevel.Visible = False
        '
        'cbCasterLevel
        '
        Me.cbCasterLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCasterLevel.FormattingEnabled = True
        Me.cbCasterLevel.Location = New System.Drawing.Point(220, 367)
        Me.cbCasterLevel.Name = "cbCasterLevel"
        Me.cbCasterLevel.Size = New System.Drawing.Size(45, 21)
        Me.cbCasterLevel.TabIndex = 43
        Me.cbCasterLevel.Visible = False
        '
        'lblSpellsKnown
        '
        Me.lblSpellsKnown.AutoSize = True
        Me.lblSpellsKnown.Location = New System.Drawing.Point(286, 371)
        Me.lblSpellsKnown.Name = "lblSpellsKnown"
        Me.lblSpellsKnown.Size = New System.Drawing.Size(72, 13)
        Me.lblSpellsKnown.TabIndex = 42
        Me.lblSpellsKnown.Text = "Spell Sources"
        Me.lblSpellsKnown.Visible = False
        '
        'cbSpellSources
        '
        Me.cbSpellSources.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSpellSources.FormattingEnabled = True
        Me.cbSpellSources.Location = New System.Drawing.Point(364, 366)
        Me.cbSpellSources.Name = "cbSpellSources"
        Me.cbSpellSources.Size = New System.Drawing.Size(45, 21)
        Me.cbSpellSources.TabIndex = 43
        Me.cbSpellSources.Visible = False
        '
        'chkSingleSpell
        '
        Me.chkSingleSpell.AutoSize = True
        Me.chkSingleSpell.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSingleSpell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSingleSpell.Location = New System.Drawing.Point(434, 368)
        Me.chkSingleSpell.Name = "chkSingleSpell"
        Me.chkSingleSpell.Size = New System.Drawing.Size(92, 19)
        Me.chkSingleSpell.TabIndex = 44
        Me.chkSingleSpell.Text = "Single Spell"
        Me.chkSingleSpell.UseVisualStyleBackColor = True
        Me.chkSingleSpell.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(32, 102)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(67, 15)
        Me.Label16.TabIndex = 45
        Me.Label16.Text = "Faction(s): "
        '
        'txtFactions
        '
        Me.txtFactions.Location = New System.Drawing.Point(178, 99)
        Me.txtFactions.Multiline = True
        Me.txtFactions.Name = "txtFactions"
        Me.txtFactions.ReadOnly = True
        Me.txtFactions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFactions.Size = New System.Drawing.Size(281, 38)
        Me.txtFactions.TabIndex = 46
        '
        'btnFactions
        '
        Me.btnFactions.Location = New System.Drawing.Point(105, 99)
        Me.btnFactions.Name = "btnFactions"
        Me.btnFactions.Size = New System.Drawing.Size(67, 25)
        Me.btnFactions.TabIndex = 47
        Me.btnFactions.Text = "Select"
        Me.btnFactions.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(505, 760)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 35)
        Me.Label19.TabIndex = 48
        Me.Label19.Text = "Cost to Heal"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblHealCost
        '
        Me.lblHealCost.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHealCost.Location = New System.Drawing.Point(519, 782)
        Me.lblHealCost.Name = "lblHealCost"
        Me.lblHealCost.Size = New System.Drawing.Size(45, 28)
        Me.lblHealCost.TabIndex = 49
        Me.lblHealCost.Text = "0"
        Me.lblHealCost.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'linkTroopCard
        '
        Me.linkTroopCard.AutoSize = True
        Me.linkTroopCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.5!)
        Me.linkTroopCard.Location = New System.Drawing.Point(102, 803)
        Me.linkTroopCard.Name = "linkTroopCard"
        Me.linkTroopCard.Size = New System.Drawing.Size(316, 16)
        Me.linkTroopCard.TabIndex = 51
        Me.linkTroopCard.TabStop = True
        Me.linkTroopCard.Text = "Click to view troop card (opens in a browser window)"
        '
        'lblToWound
        '
        Me.lblToWound.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToWound.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToWound.Location = New System.Drawing.Point(508, 594)
        Me.lblToWound.Name = "lblToWound"
        Me.lblToWound.Size = New System.Drawing.Size(69, 28)
        Me.lblToWound.TabIndex = 53
        Me.lblToWound.Text = "0"
        Me.lblToWound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(495, 562)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(94, 32)
        Me.Label23.TabIndex = 52
        Me.Label23.Text = "To Wound (First / Additional)"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolStrip1
        '
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButtonNewTroopList, Me.ToolStripButtonOpenTroopList, Me.ToolStripButtonSaveCurrentTroop, Me.ToolStripSeparator2, Me.ToolStripButtonLoadFromTroopList, Me.ToolStripButtonLoadFromGlobalTroopList})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(611, 25)
        Me.ToolStrip1.TabIndex = 54
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButtonNewTroopList
        '
        Me.ToolStripButtonNewTroopList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonNewTroopList.Image = CType(resources.GetObject("ToolStripButtonNewTroopList.Image"), System.Drawing.Image)
        Me.ToolStripButtonNewTroopList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonNewTroopList.Name = "ToolStripButtonNewTroopList"
        Me.ToolStripButtonNewTroopList.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonNewTroopList.Text = "New Troop List"
        Me.ToolStripButtonNewTroopList.ToolTipText = "Create a new troop list file"
        '
        'ToolStripButtonOpenTroopList
        '
        Me.ToolStripButtonOpenTroopList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonOpenTroopList.Image = CType(resources.GetObject("ToolStripButtonOpenTroopList.Image"), System.Drawing.Image)
        Me.ToolStripButtonOpenTroopList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonOpenTroopList.Name = "ToolStripButtonOpenTroopList"
        Me.ToolStripButtonOpenTroopList.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonOpenTroopList.Text = "Open Troop List"
        '
        'ToolStripButtonSaveCurrentTroop
        '
        Me.ToolStripButtonSaveCurrentTroop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonSaveCurrentTroop.Enabled = False
        Me.ToolStripButtonSaveCurrentTroop.Image = CType(resources.GetObject("ToolStripButtonSaveCurrentTroop.Image"), System.Drawing.Image)
        Me.ToolStripButtonSaveCurrentTroop.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonSaveCurrentTroop.Name = "ToolStripButtonSaveCurrentTroop"
        Me.ToolStripButtonSaveCurrentTroop.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonSaveCurrentTroop.Text = "Save Current Troop"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButtonLoadFromTroopList
        '
        Me.ToolStripButtonLoadFromTroopList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonLoadFromTroopList.Enabled = False
        Me.ToolStripButtonLoadFromTroopList.Image = CType(resources.GetObject("ToolStripButtonLoadFromTroopList.Image"), System.Drawing.Image)
        Me.ToolStripButtonLoadFromTroopList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonLoadFromTroopList.Name = "ToolStripButtonLoadFromTroopList"
        Me.ToolStripButtonLoadFromTroopList.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonLoadFromTroopList.Text = "Load From Troop List"
        '
        'ToolStripButtonLoadFromGlobalTroopList
        '
        Me.ToolStripButtonLoadFromGlobalTroopList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonLoadFromGlobalTroopList.Image = CType(resources.GetObject("ToolStripButtonLoadFromGlobalTroopList.Image"), System.Drawing.Image)
        Me.ToolStripButtonLoadFromGlobalTroopList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonLoadFromGlobalTroopList.Name = "ToolStripButtonLoadFromGlobalTroopList"
        Me.ToolStripButtonLoadFromGlobalTroopList.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonLoadFromGlobalTroopList.Text = "Load From Global Troop List"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(611, 835)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.lblToWound)
        Me.Controls.Add(Me.linkTroopCard)
        Me.Controls.Add(Me.lblHealCost)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.btnFactions)
        Me.Controls.Add(Me.txtFactions)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.chkSingleSpell)
        Me.Controls.Add(Me.cbSpellSources)
        Me.Controls.Add(Me.cbCasterLevel)
        Me.Controls.Add(Me.lblSpellsKnown)
        Me.Controls.Add(Me.lblCasterLevel)
        Me.Controls.Add(Me.chkSpellCaster)
        Me.Controls.Add(Me.btnBasicAttacks)
        Me.Controls.Add(Me.btnCustomAttacks)
        Me.Controls.Add(Me.btnWoundsUp)
        Me.Controls.Add(Me.btnWoundsDn)
        Me.Controls.Add(Me.txtWounds)
        Me.Controls.Add(Me.btnRangedSkillUp)
        Me.Controls.Add(Me.btnRangedSkillDn)
        Me.Controls.Add(Me.btnMeleeSkillUp)
        Me.Controls.Add(Me.txtRangedSkill)
        Me.Controls.Add(Me.btnMeleeSkillDn)
        Me.Controls.Add(Me.txtMeleeSkill)
        Me.Controls.Add(Me.btnStrengthUp)
        Me.Controls.Add(Me.btnStrengthDn)
        Me.Controls.Add(Me.txtStrength)
        Me.Controls.Add(Me.btnMoveUp)
        Me.Controls.Add(Me.btnMoveDn)
        Me.Controls.Add(Me.txtMove)
        Me.Controls.Add(Me.txtArmorName)
        Me.Controls.Add(Me.chkArmorName)
        Me.Controls.Add(Me.lblRangedDef)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.lblMeleeDef)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnQualities)
        Me.Controls.Add(Me.txtTroopQualities)
        Me.Controls.Add(Me.lblPointValue)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.radOffhandBoth)
        Me.Controls.Add(Me.radOffHandSec)
        Me.Controls.Add(Me.radOffHandBonus)
        Me.Controls.Add(Me.lblOffHandWpns)
        Me.Controls.Add(Me.chkShield)
        Me.Controls.Add(Me.txtAttacks)
        Me.Controls.Add(Me.txtRangedDesc)
        Me.Controls.Add(Me.txtMeleeOffDesc)
        Me.Controls.Add(Me.txtMeleePriDesc)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cbRangedWeapons)
        Me.Controls.Add(Me.cbMeleeWpnsOff)
        Me.Controls.Add(Me.cbMeleeWpnsPri)
        Me.Controls.Add(Me.txtTroopName)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.cbSize)
        Me.Controls.Add(Me.cbTemplate)
        Me.Controls.Add(Me.cbRace)
        Me.Controls.Add(Me.cbArmor)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Label23)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "Sword and Claw Troop Generator 1.67"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbArmor As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbSize As System.Windows.Forms.ComboBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTroopName As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbRace As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbTemplate As System.Windows.Forms.ComboBox
    Friend WithEvents cbMeleeWpnsPri As System.Windows.Forms.ComboBox
    Friend WithEvents cbRangedWeapons As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cbMeleeWpnsOff As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtMeleePriDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtMeleeOffDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtRangedDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtAttacks As System.Windows.Forms.TextBox
    Friend WithEvents chkShield As System.Windows.Forms.CheckBox
    Friend WithEvents lblOffHandWpns As System.Windows.Forms.Label
    Friend WithEvents radOffHandBonus As System.Windows.Forms.RadioButton
    Friend WithEvents radOffHandSec As System.Windows.Forms.RadioButton
    Friend WithEvents radOffhandBoth As System.Windows.Forms.RadioButton
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblPointValue As System.Windows.Forms.Label
    Friend WithEvents txtTroopQualities As System.Windows.Forms.TextBox
    Friend WithEvents btnQualities As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblMeleeDef As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lblRangedDef As System.Windows.Forms.Label
    Friend WithEvents chkArmorName As System.Windows.Forms.CheckBox
    Friend WithEvents txtArmorName As System.Windows.Forms.TextBox
    Friend WithEvents btnMoveDn As System.Windows.Forms.Button
    Friend WithEvents txtMove As System.Windows.Forms.TextBox
    Friend WithEvents btnMoveUp As System.Windows.Forms.Button
    Friend WithEvents btnStrengthUp As System.Windows.Forms.Button
    Friend WithEvents btnStrengthDn As System.Windows.Forms.Button
    Friend WithEvents txtStrength As System.Windows.Forms.TextBox
    Friend WithEvents btnMeleeSkillUp As System.Windows.Forms.Button
    Friend WithEvents btnMeleeSkillDn As System.Windows.Forms.Button
    Friend WithEvents txtMeleeSkill As System.Windows.Forms.TextBox
    Friend WithEvents txtRangedSkill As System.Windows.Forms.TextBox
    Friend WithEvents btnRangedSkillDn As System.Windows.Forms.Button
    Friend WithEvents btnRangedSkillUp As System.Windows.Forms.Button
    Friend WithEvents btnWoundsUp As System.Windows.Forms.Button
    Friend WithEvents btnWoundsDn As System.Windows.Forms.Button
    Friend WithEvents txtWounds As System.Windows.Forms.TextBox
    Friend WithEvents btnCustomAttacks As System.Windows.Forms.Button
    Friend WithEvents btnBasicAttacks As System.Windows.Forms.Button
    Friend WithEvents CheckForUpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuOpenTroopList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuSaveCurrent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuLoadFromTroopList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuLoadFromGlobalTroopList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuNewTroopList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuPrintSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuPrintCurrentTroop As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuPrintArmyList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents menuPrintGlobalTroopList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SortByNameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SortByPointValueToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkSpellCaster As System.Windows.Forms.CheckBox
    Friend WithEvents lblCasterLevel As System.Windows.Forms.Label
    Friend WithEvents cbCasterLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblSpellsKnown As System.Windows.Forms.Label
    Friend WithEvents cbSpellSources As System.Windows.Forms.ComboBox
    Friend WithEvents chkSingleSpell As System.Windows.Forms.CheckBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtFactions As TextBox
    Friend WithEvents btnFactions As Button
    Friend WithEvents Label19 As Label
    Friend WithEvents lblHealCost As Label
    Friend WithEvents ToolsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents menuRecalculateAllInTroopList As ToolStripMenuItem
    Friend WithEvents linkTroopCard As LinkLabel
    Friend WithEvents lblToWound As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents ToolStripButtonNewTroopList As ToolStripButton
    Friend WithEvents ToolStripButtonOpenTroopList As ToolStripButton
    Friend WithEvents ToolStripButtonSaveCurrentTroop As ToolStripButton
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ToolStripButtonLoadFromTroopList As ToolStripButton
    Friend WithEvents ToolStripButtonLoadFromGlobalTroopList As ToolStripButton
End Class
