﻿Imports System.Xml, System.Web

Public Structure Race
    Public name As String
    Public move As Integer
    Public wounds As Integer
    Public meleeSkill As Integer
    Public rangedSkill As Integer
    Public str As Integer
    Public size As String
    Public specialAbilities As List(Of String)
    Public armorMod As Integer
    Public armorDesc As String
    Public weapon As String
    Public Sub New(newName As String)
        Me.name = newName
        Me.move = 4
        Me.wounds = 2
        Me.meleeSkill = 0
        Me.rangedSkill = 0
        Me.str = 0
        Me.size = "Medium"
        Me.armorMod = 0
        Me.weapon = "Sword"
    End Sub
End Structure

Public Structure Weapon
    Public name As String
    Public type As String
    Public dmgDie As Integer
    Public parry As Integer
    Public range As Integer
    Public strMod As String
    Public qualities As List(Of String)
    'Public Function hasQuality(quality As String) As Boolean
    '    If Me.qualities.Length = 0 Then Return False
    'Dim i = 0
    '    While i < Me.qualities.
    '        If Me.qualities(i).ToLower = quality.ToLower Then Return True
    '        i += 1
    '    End While
    '    Return False
    'End Function
    'Public Sub addQuality(quality As String)
    ' Dim i As Integer = Me.qualities.Length
    '     MessageBox.Show(i.ToString + " " + Me.qualities.Length.ToString)
    '     ReDim Preserve Me.qualities(i + 1)
    '     Me.qualities(i) = quality
    '
End Structure
Public Structure StatType
    Public name As String
    Public min As Integer
    Public max As Integer
End Structure
Public Structure AttackType
    Public name As String
    Public type As String
    Public toHit As Integer
    Public dmgDie As Integer
    Public dmgMod As Integer
    Public numAttacks As Integer
    Public range As Integer
    Public pointValue As Double
    Public expectedHits As Double
    Public expectedWounds As Double
    Public qualities As List(Of String)
End Structure
Public Structure AttackArrayType
    Public name As String
    Public type As String
    Public attacks As List(Of AttackType)
    Public pointValue As Double
End Structure
Public Structure OffenseType
    Public attackArrays As List(Of AttackArrayType)
    Public pointValue As Double
    Public pointValueMelee As Double
    Public pointValueRanged As Double
End Structure
Public Structure ModifierType
    Public name As String
    Public value As Double
End Structure
Public Structure QualityType
    Public name As String
    Public cost As Single
    Public type As String
    Public modifiers As List(Of ModifierType)
End Structure
Public Structure TroopType
    Public name As String
    Public pointValue As String
    Public race As String
    Public template As String
    Public move As String
    Public str As String
    Public meleeSkill As String
    Public rangedSkill As String
    Public armor As String
    Public armorCustom As Boolean
    Public armorCustomName As String
    Public shield As Boolean
    Public wounds As String
    Public size As String
    Public qualitiesString As String
    Public qualitiesList As List(Of QualityType)
    Public weaponPrimary As String
    Public weaponOffhand As String
    Public weaponRanged As String
    Public offhandOptions As Integer
    Public customAttacks As Boolean
    Public customParryMod As Integer
    Public DefenseMelee As String
    Public DefenseRanged As String
    Public DefenseArmor As Integer
    Public spellcaster As Boolean
    Public spellLevel As Double
    Public spellSourcesKnown As Double
    Public spellSingle As Boolean
    Public factions As List(Of String)
    Public factionString As String
    Public offense As OffenseType
    Public healCost As String
    Public toWoundFirst As Integer
    Public toWoundMore As Integer

End Structure
Public Class frmMain

    Public races(1) As Race
    Public templates(1) As Race
    Public stats As List(Of StatType) = New List(Of StatType)
    Public weapons As List(Of Weapon) = New List(Of Weapon)
    Public wpnQualities As List(Of QualityType) = New List(Of QualityType)
    Public troopQualities As List(Of QualityType) = New List(Of QualityType)
    Public selectedTroopQualities As List(Of QualityType) = New List(Of QualityType)
    'Dim attacks As List(Of AttackArrayType) = New List(Of AttackArrayType)
    Public offense As OffenseType = New OffenseType
    Public defense As Double
    Public defenseWndsPerTurnRanged As Double
    Public defenseWndsPerTurnMelee As Double
    Public magicCost As Double
    Public Initializing As Boolean = 0
    Public CustomAttacks As Boolean = False
    Public CustomAttackParryMod As Integer
    Public AppDirectory As String = My.Application.Info.DirectoryPath
    Public SaCDataVersion, GlobalTroopListVersion As Double
    Public GlobalTroopListFile As String
    Public UserTroopXMLDoc As New XmlDocument
    Public UserTroopXMLroot As XmlNode
    Public UserTroopFileName As String
    Public SpellCostperLevel As Double
    'Public SpellCostModatLevel As Double()
    Public SpellSources As Double
    Public factions As List(Of String) = New List(Of String)
    Public selectedFactions As List(Of String) = New List(Of String)
    Public webserverLocalAddr As String
    Public webserverLocalTF As Boolean
    Public toWoundFirst As Integer = 4
    Public toWoundMore As Integer = 4
    Public lastTroopFromGlobal As String = ""
    Public lastTroopFromLocal As String = ""







    Public Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Initializing = True
        CheckForConfigFiles()
        offense.attackArrays = New List(Of AttackArrayType)
        txtTroopName.Text = "Human Soldier"
        Import_Files()
        avgtroop = New basicOpponent("average")
        races(0) = New Race("Human")
        cbRace.SelectedItem = "Human"
        Reset_Stats()
        'cbTemplate.SelectedItem = "Soldier"
        'MessageBox.Show(Application.StartupPath)

        Initializing = False
        'GenerateBasicAttackArrays()
        RefreshCalculations()

        'UpdatePointValue()
    End Sub
    Public Sub CheckForConfigFiles()
        If Not (IO.File.Exists(AppDirectory + "\SaCGenData.xml") And
                IO.File.Exists(AppDirectory + "\GlobalTroopList.xml")) Then

            MessageBox.Show("One of the configuration files was not found.  This program will check for the latest versions.", "Sword and Claw Troop Generator")
            CheckForUpdates()
            MessageBox.Show("This program will now exit.  Please restart to begin using it.", "Sword and Claw Troop Generator")
            Me.Close()
        End If

    End Sub
    Public Sub Reset_Stats()

        cbArmor.SelectedIndex = 2
        'cbSize.SelectedIndex = 1
        'cbMove.SelectedItem = "4"
        'cbSTR.SelectedItem = "0"
        'cbWounds.SelectedItem = "2"
        'cbMeleeSkill.SelectedItem = "0"
        'cbRangedSkill.SelectedItem = "0"
        cbRace.SelectedItem = "Human"
        cbTemplate.SelectedItem = "Soldier"
        ResetToRaceDefaults("Human")
        RevertFormToBasicAttacks()
        chkArmorName.Checked = False
        chkShield.Checked = False
        chkSpellCaster.Checked = False
        cbSpellSources.SelectedIndex = 0
        cbCasterLevel.SelectedIndex = 0
        cbMeleeWpnsPri.SelectedItem = "Sword"
        cbMeleeWpnsOff.SelectedItem = "<None>"
        cbRangedWeapons.SelectedItem = "<None>"
        magicCost = 0

        RefreshCalculations()

    End Sub

    Public Sub Import_Files()
        ' import standard race stats, weapons
        ' import stats database.  Have it check online (my dropbox) for a new file first
        ' import local stats database
        Dim numRaces = 0
        Dim numTemplates = 0
        Dim racename, templateName As String
        If (IO.File.Exists(AppDirectory + "\SaCGenData.xml")) Then

            GlobalTroopListFile = AppDirectory + "\GlobalTroopList.xml"
            SaCDataVersion = XMLFileVersion(AppDirectory + "\SaCGenData.xml")
            GlobalTroopListVersion = XMLFileVersion(GlobalTroopListFile)
            Dim xmlDoc As New XmlDocument
            xmlDoc.Load(AppDirectory + "\SaCGenData.xml")
            Dim xmlroot As XmlNode = xmlDoc.DocumentElement

            Try
                Dim webserverElement As XmlElement = xmlroot.SelectSingleNode("webserver-local")
                webserverLocalAddr = webserverElement.GetAttribute("address")
                webserverLocalTF = webserverElement.GetAttribute("uselocal")
            Catch
                webserverLocalTF = False
            End Try

            Dim raceList As XmlNodeList = xmlroot.SelectNodes("//SaCData/race")
            For Each raceItem As XmlElement In raceList
                racename = raceItem.GetAttribute("name")
                cbRace.Items.Add(racename)
                numRaces += 1
                If numRaces >= races.Length Then
                    ReDim Preserve races(numRaces + 10)
                End If
                races(numRaces) = New Race(racename)
                If raceItem.HasAttribute("armorDesc") Then races(numRaces).armorDesc = raceItem.GetAttribute("armorDesc")
                If raceItem.HasAttribute("armorMod") Then races(numRaces).armorMod = raceItem.GetAttribute("armorMod")
                If raceItem.HasAttribute("meleeSkill") Then races(numRaces).meleeSkill = raceItem.GetAttribute("meleeSkill")
                If raceItem.HasAttribute("move") Then races(numRaces).move = raceItem.GetAttribute("move")
                If raceItem.HasAttribute("rangedSkill") Then races(numRaces).rangedSkill = raceItem.GetAttribute("rangedSkill")
                If raceItem.HasAttribute("size") Then races(numRaces).size = raceItem.GetAttribute("size")
                If raceItem.HasAttribute("strength") Then races(numRaces).str = raceItem.GetAttribute("strength")
                If raceItem.HasAttribute("wounds") Then races(numRaces).wounds = raceItem.GetAttribute("wounds")
                'If raceItem.HasAttribute("specialAbilities") Then races(numRaces).specialAbilities = raceItem.GetAttribute("specialAbilities")
                Dim qualityList As XmlNodeList = raceItem.SelectNodes("quality")
                races(numRaces).specialAbilities = New List(Of String)
                For Each qualityItem As XmlElement In qualityList
                    races(numRaces).specialAbilities.Add(qualityItem.InnerText)
                Next
            Next

            Dim templateList As XmlNodeList = xmlroot.SelectNodes("//SaCData/template")
            For Each templateItem As XmlElement In templateList
                templateName = templateItem.GetAttribute("name")
                cbTemplate.Items.Add(templateName)
                numTemplates += 1
                If numTemplates >= templates.Length Then
                    ReDim Preserve templates(numTemplates + 10)
                End If
                'templates(numtemplates) = New Race(racename)
                templates(numTemplates).name = templateName
                If templateItem.HasAttribute("armorDesc") Then templates(numTemplates).armorDesc = templateItem.GetAttribute("armorDesc")
                If templateItem.HasAttribute("armorMod") Then templates(numTemplates).armorMod = templateItem.GetAttribute("armorMod")
                If templateItem.HasAttribute("meleeSkill") Then templates(numTemplates).meleeSkill = templateItem.GetAttribute("meleeSkill")
                If templateItem.HasAttribute("move") Then templates(numTemplates).move = templateItem.GetAttribute("move")
                If templateItem.HasAttribute("rangedSkill") Then templates(numTemplates).rangedSkill = templateItem.GetAttribute("rangedSkill")
                If templateItem.HasAttribute("size") Then templates(numTemplates).size = templateItem.GetAttribute("size")
                If templateItem.HasAttribute("strength") Then templates(numTemplates).str = templateItem.GetAttribute("strength")
                If templateItem.HasAttribute("wounds") Then templates(numTemplates).wounds = templateItem.GetAttribute("wounds")
                'If templateItem.HasAttribute("specialAbilities") Then templates(numTemplates).specialAbilities = templateItem.GetAttribute("specialAbilities")
                Dim qualityList As XmlNodeList = templateItem.SelectNodes("quality")
                templates(numTemplates).specialAbilities = New List(Of String)
                For Each qualityItem As XmlElement In qualityList
                    templates(numTemplates).specialAbilities.Add(qualityItem.InnerText)
                Next

            Next
            cbTemplate.SelectedItem = "Soldier"

            Dim statList As XmlNodeList = xmlroot.SelectNodes("//SaCData/stat")
            Dim stat As StatType
            For Each statItem As XmlElement In statList
                stat.name = statItem.GetAttribute("name")
                stat.min = CInt(statItem.GetAttribute("min"))
                stat.max = CInt(statItem.GetAttribute("max"))
                stats.Add(stat)
            Next

            Dim facList As XmlNodeList = xmlroot.SelectNodes("//SaCData/faction")
            Dim fac As String
            For Each facItem As XmlElement In facList
                fac = facItem.GetAttribute("name")
                factions.Add(fac)
            Next

            Dim magicList As XmlNodeList = xmlroot.SelectNodes("//SaCData/magic")
            For Each magicItem As XmlElement In magicList
                Dim magicStat As StatType

                magicStat.min = CInt(magicItem.GetAttribute("minlevel"))
                magicStat.max = CInt(magicItem.GetAttribute("maxlevel"))
                SpellCostperLevel = CDbl(magicItem.GetAttribute("costperlevel"))
                SpellSources = CInt(magicItem.GetAttribute("spellsourcesmax"))
                'ReDim SpellCostModatLevel(SpellSources + 1)

                For i As Integer = magicStat.min To magicStat.max
                    cbCasterLevel.Items.Add(i)
                Next
                cbCasterLevel.SelectedIndex = 0
                For i As Integer = 1 To SpellSources
                    cbSpellSources.Items.Add(i)
                    'SpellCostModatLevel(i) = CDbl(magicItem.GetAttribute("spells" + i.ToString + "xlevel"))
                Next
                cbSpellSources.SelectedIndex = 0

                Exit For
            Next


            Dim weaponName As String
            Dim numweapons As Integer = 0
            Dim wpn As Weapon
            Dim weaponList As XmlNodeList = xmlroot.SelectNodes("//SaCData/weapon")
            For Each weaponItem As XmlElement In weaponList
                weaponName = weaponItem.GetAttribute("name")
                numweapons += 1
                wpn = New Weapon
                'If numweapons >= weapons.Length Then
                'ReDim Preserve weapons(numweapons + 10)
                'End If
                wpn.name = weaponName
                If weaponItem.HasAttribute("type") Then wpn.type = weaponItem.GetAttribute("type")
                If weaponItem.HasAttribute("dmgDie") Then wpn.dmgDie = weaponItem.GetAttribute("dmgDie")
                If weaponItem.HasAttribute("parry") Then wpn.parry = weaponItem.GetAttribute("parry")
                If weaponItem.HasAttribute("range") Then wpn.range = weaponItem.GetAttribute("range")
                If weaponItem.HasAttribute("strMod") Then wpn.strMod = weaponItem.GetAttribute("strMod")
                Dim qualityList As XmlNodeList = weaponItem.SelectNodes("quality")
                wpn.qualities = New List(Of String)
                For Each qualityItem As XmlElement In qualityList
                    wpn.qualities.Add(qualityItem.InnerText)
                Next
                If wpn.type.ToLower = "ranged" Then
                    cbRangedWeapons.Items.Add(weaponName)
                Else
                    cbMeleeWpnsPri.Items.Add(weaponName)
                    'Find(Function(x) x.name = wpnStr)
                    If wpn.parry >= 0 And wpn.qualities.Find(Function(x) x.ToLower = "two-handed") = Nothing Then
                        cbMeleeWpnsOff.Items.Add(weaponName)
                    End If
                End If
                weapons.Add(wpn)
            Next

            'Add a <None> weapon for offhand and ranged.
            wpn = New Weapon
            wpn.name = "<None>"
            weapons.Add(wpn)
            cbMeleeWpnsOff.Items.Add("<None>")
            cbMeleeWpnsOff.SelectedItem = "<None>"
            cbRangedWeapons.Items.Add("<None>")
            cbRangedWeapons.SelectedItem = "<None>"
            cbMeleeWpnsPri.SelectedItem = "Sword"

            Dim qual As QualityType = New QualityType
            Dim wpnQualitieslist As XmlNodeList = xmlroot.SelectNodes("//SaCData/weapon_quality")
            For Each qualityitem As XmlElement In wpnQualitieslist
                qual.name = qualityitem.GetAttribute("name")
                qual.cost = CSng(qualityitem.GetAttribute("cost"))
                'qual.type = qualityitem.GetAttribute("type")
                wpnQualities.Add(qual)
            Next


            qual = New QualityType
            Dim tmod As New ModifierType
            Dim troopQualitieslist As XmlNodeList = xmlroot.SelectNodes("//SaCData/troop_quality")
            For Each qualityitem As XmlElement In troopQualitieslist
                qual = New QualityType
                qual.modifiers = New List(Of ModifierType)
                qual.name = qualityitem.GetAttribute("name")
                qual.cost = CSng(qualityitem.GetAttribute("cost"))
                'qual.type = qualityitem.GetAttribute("type")

                Dim modList As XmlNodeList = qualityitem.SelectNodes("modifier")
                For Each modItem As XmlElement In modList
                    tmod.name = modItem.GetAttribute("name")
                    tmod.value = modItem.GetAttribute("value")
                    qual.modifiers.Add(tmod)
                Next

                troopQualities.Add(qual)
            Next
        Else

            MessageBox.Show("The filename you selected was not found.")
        End If
    End Sub

    Public Sub UpdateQualityModifiers()
        Dim q As QualityType
        Dim qualities As List(Of QualityType) = New List(Of QualityType)
        For Each qual In selectedTroopQualities
            q = troopQualities.Find(Function(x) x.name.ToLower = qual.name.ToLower)
            qualities.Add(q)
        Next
        selectedTroopQualities = qualities
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Reset_Stats()

    End Sub
    Public Sub ResetSpecialQualities(myraceName As String, tempName As String)
        selectedTroopQualities = New List(Of QualityType)
        txtTroopQualities.Text = ""

        Dim myrace As Race = New Race
        Dim temp As Race = New Race

        Dim found As Boolean = False
        Dim i As Integer = 0
        Do While Not found And i <= races.Length
            If races(i).name = myraceName Then
                found = True
                myrace = races(i)
            Else : i += 1
            End If
        Loop

        found = False
        i = 0
        Do While Not found And i <= templates.Length
            If templates(i).name = tempName Then
                found = True
                temp = templates(i)
            Else : i += 1
            End If
        Loop

        Dim qual As QualityType = New QualityType
        If Not IsNothing(myrace.specialAbilities) Then
            For Each qualStr As String In myrace.specialAbilities
                qual = troopQualities.Find(Function(x) x.name.ToLower = qualStr.ToLower)
                selectedTroopQualities.Add(qual)
                If txtTroopQualities.Text <> "" Then txtTroopQualities.Text += "; "
                txtTroopQualities.Text += qualStr
            Next
        End If

        If Not IsNothing(temp.specialAbilities) Then
            For Each qualStr As String In temp.specialAbilities
                qual = troopQualities.Find(Function(x) x.name.ToLower = qualStr.ToLower)
                selectedTroopQualities.Add(qual)
                If txtTroopQualities.Text <> "" Then txtTroopQualities.Text += "; "
                txtTroopQualities.Text += qualStr
            Next
        End If


    End Sub
    Public Sub ResetToRaceDefaults(newrace As String)
        Dim found As Boolean = False
        Dim i As Integer = 0
        Do While Not found And i <= races.Length
            If races(i).name = newrace Then
                found = True
            Else : i += 1
            End If
        Loop
        If Not found Then
            MessageBox.Show("Error: The selected race could not be found.")
            Exit Sub
        End If
        'cbSTR.SelectedItem = races(i).str.ToString
        'cbArmor.SelectedItem = races(i).armorMod
        'cbMeleeSkill.SelectedItem = races(i).meleeSkill.ToString
        'cbMove.SelectedItem = races(i).move.ToString
        'cbRangedSkill.SelectedItem = races(i).rangedSkill.ToString
        cbSize.SelectedItem = races(i).size
        'cbWounds.SelectedItem = races(i).wounds.ToString

        txtMove.Text = races(i).move.ToString
        txtStrength.Text = races(i).str.ToString
        txtMeleeSkill.Text = races(i).meleeSkill.ToString
        txtRangedSkill.Text = races(i).rangedSkill.ToString
        txtWounds.Text = races(i).wounds.ToString

        txtTroopName.Text = cbRace.SelectedItem
        If cbTemplate.SelectedItem <> "<None>" Then txtTroopName.Text += " " + cbTemplate.SelectedItem

        ResetSpecialQualities(newrace, cbTemplate.SelectedItem)
    End Sub
    Private Sub cbRace_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbRace.SelectedIndexChanged
        If Initializing Then
            Exit Sub
        End If

        If cbRace.SelectedItem.ToLower = "<none>" Then
            Exit Sub
        End If

        'Reset_Stats()
        ResetToRaceDefaults(cbRace.SelectedItem)
        ApplyTemplate(cbTemplate.SelectedItem)
    End Sub
    Private Sub cbTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTemplate.SelectedIndexChanged
        If Initializing Then
            Exit Sub
        End If

        ApplyTemplate(cbTemplate.SelectedItem)
    End Sub
    Public Sub adjustStat(txtbx As TextBox, statName As String, diff As Integer)
        Dim stat As StatType = stats.Find(Function(x) x.name.ToLower = statName.ToLower)
        If diff < 0 Then
            If CInt(txtbx.Text) + diff < stat.min Then
                txtbx.Text = stat.min.ToString
            Else
                txtbx.Text = (CInt(txtbx.Text) + diff).ToString
            End If
        ElseIf diff > 0 Then
            If CInt(txtbx.Text) + diff > stat.max Then
                txtbx.Text = stat.max.ToString
            Else
                txtbx.Text = (CInt(txtbx.Text) + diff).ToString
            End If
        End If
        RefreshCalculations()
    End Sub
    Public Sub adjust(cb As ComboBox, diff As Integer)
        If diff < 0 Then
            If cb.SelectedIndex + diff > cb.Items.Count - 1 Then
                cb.SelectedIndex = cb.Items.Count - 1
            Else
                cb.SelectedIndex -= diff

            End If
        ElseIf diff > 0 Then
            If cb.SelectedIndex - diff < 0 Then
                cb.SelectedIndex = 0
            Else
                cb.SelectedIndex -= diff
            End If

        End If
    End Sub
    Public Sub ApplyTemplate(newTemplate As String)
        Dim found As Boolean = False
        Dim i As Integer = 0
        Do While Not found And i <= templates.Length
            If templates(i).name = newTemplate Then
                found = True
            Else : i += 1
            End If
        Loop
        If Not found Then
            MessageBox.Show("Error: The selected template could not be found.")
            Exit Sub
        End If
        ResetToRaceDefaults(cbRace.SelectedItem)
        'If Not IsNothing(templates(i).str) Then adjust(cbSTR, templates(i).str)
        'If Not IsNothing(templates(i).meleeSkill) Then adjust(cbMeleeSkill, templates(i).meleeSkill)
        'If Not IsNothing(templates(i).rangedSkill) Then adjust(cbRangedSkill, templates(i).rangedSkill)
        'If Not IsNothing(templates(i).move) Then adjust(cbMove, templates(i).move)
        'If Not IsNothing(templates(i).wounds) Then adjust(cbWounds, templates(i).wounds)

        If Not IsNothing(templates(i).move) Then adjustStat(txtMove, "move", templates(i).move)
        If Not IsNothing(templates(i).str) Then adjustStat(txtStrength, "strength", templates(i).str)
        If Not IsNothing(templates(i).meleeSkill) Then adjustStat(txtMeleeSkill, "meleeskill", templates(i).meleeSkill)
        If Not IsNothing(templates(i).rangedSkill) Then adjustStat(txtRangedSkill, "rangedskill", templates(i).rangedSkill)
        If Not IsNothing(templates(i).wounds) Then adjustStat(txtWounds, "wounds", templates(i).wounds)

    End Sub
    Public Sub FillWeaponDescription(wpn As Weapon, dest As TextBox)
        Dim txt As String
        If wpn.name = "<None>" Then
            dest.Text = ""
            Exit Sub
        End If
        txt = "Dmg: d" + wpn.dmgDie.ToString
        If wpn.strMod <> Nothing Then
            txt += " + " + wpn.strMod
        Else
            txt += " + Str"
        End If
        If wpn.parry <> Nothing Then
            txt += ", Parry: "
            If wpn.parry > 0 Then txt += "+"
            txt += wpn.parry.ToString()
        End If
        If wpn.type.ToLower = "ranged" Then
            txt += ", Range: " + "(" + wpn.range.ToString + "/" + (wpn.range * 2).ToString + "/" + (wpn.range * 3).ToString + ")"
        End If
        txt += ".  "
        If wpn.qualities.Count > 0 Then
            txt += "Special: "
            For Each qual As String In wpn.qualities
                txt += qual + ", "
            Next
            txt = txt.Substring(0, txt.Length - 2)
        End If
        dest.Text = txt

    End Sub
    Private Sub cbMeleeWeapons_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbMeleeWpnsPri.SelectedIndexChanged, cbRangedWeapons.SelectedIndexChanged, cbMeleeWpnsOff.SelectedIndexChanged
        Dim wpnStr As String = sender.SelectedItem
        Dim wpn As Weapon = weapons.Find(Function(x) x.name = wpnStr)
        If sender.Equals(cbMeleeWpnsPri) Then
            FillWeaponDescription(wpn, txtMeleePriDesc)
        ElseIf sender.Equals(cbMeleeWpnsOff) Then
            FillWeaponDescription(wpn, txtMeleeOffDesc)
        ElseIf sender.Equals(cbRangedWeapons) Then
            FillWeaponDescription(wpn, txtRangedDesc)
        End If

        If Initializing Then Exit Sub

        'If sender.Equals(cbMeleeWpnsPri) Then
        If wpn.name <> "<None>" Then
            If wpn.qualities.Find(Function(x) x.ToLower = "two-handed") <> Nothing Then
                If chkShield.Checked Then
                    MsgBox("Since you selected a two handed weapon, the shield has been removed.", MsgBoxStyle.Information)
                    chkShield.Checked = False
                End If
                cbMeleeWpnsOff.SelectedItem = "<None>"
                chkShield.Enabled = False
                cbMeleeWpnsOff.Enabled = False
            Else
                If Not chkShield.Checked Then
                    cbMeleeWpnsOff.Enabled = True
                End If
                chkShield.Enabled = True
            End If

        Else
            If Not chkShield.Checked Then
                cbMeleeWpnsOff.Enabled = True
            End If
            chkShield.Enabled = True
        End If
        RefreshCalculations()
    End Sub



    Private Sub chkShield_CheckedChanged(sender As Object, e As EventArgs) Handles chkShield.CheckedChanged
        If CustomAttacks = True Then
            RefreshCalculations()
            Exit Sub
        End If

        If chkShield.Checked Then
            cbMeleeWpnsOff.SelectedItem = "<None>"
            cbMeleeWpnsOff.Enabled = False
            txtMeleeOffDesc.Text = "Shield"
        Else
            cbMeleeWpnsOff.Enabled = True
            txtMeleeOffDesc.Text = ""
        End If
        RefreshCalculations()
    End Sub

    Public Sub GenerateBasicAttackArrays()
        offense = New OffenseType
        offense.attackArrays = New List(Of AttackArrayType)

        Dim atkarray As AttackArrayType = New AttackArrayType
        atkarray.attacks = New List(Of AttackType)

        'Dim primaryOnly, offHandBonus, offHandSec, offHandBoth As Boolean
        Dim atk As AttackType = New AttackType
        Dim atkWBonus As AttackType = New AttackType
        Dim atksec As AttackType = New AttackType
        Dim wpn As Weapon = New Weapon
        Dim wpn2 As Weapon = New Weapon
        Dim wpnrng As Weapon = New Weapon

        Dim offhandbonus As Integer = 0

        atk.name = cbMeleeWpnsPri.SelectedItem
        atkarray.name = atk.name
        atkarray.type = "melee"
        wpn = weapons.Find(Function(x) x.name = atk.name)



        'atk = GenerateMeleeAttack(CInt(cbMeleeSkill.SelectedItem), CInt(cbSTR.SelectedItem), 1, wpnQualities, wpn, avgtroop)
        'atkWBonus = GenerateMeleeAttack(CInt(cbMeleeSkill.SelectedItem) + 1, CInt(cbSTR.SelectedItem), 1, wpnQualities, wpn, avgtroop)

        atk = GenerateMeleeAttack(CInt(txtMeleeSkill.Text), CInt(txtStrength.Text), 1, wpnQualities, wpn, avgtroop)
        atkWBonus = GenerateMeleeAttack(CInt(txtMeleeSkill.Text) + 1, CInt(txtStrength.Text), 1, wpnQualities, wpn, avgtroop)

        If cbMeleeWpnsOff.SelectedItem <> "<None>" And (radOffHandSec.Checked Or radOffhandBoth.Checked) Then

            atksec = New AttackType
            atksec.name = cbMeleeWpnsOff.SelectedItem
            wpn2 = weapons.Find(Function(x) x.name = atksec.name)
            'atksec = GenerateMeleeAttack(CInt(cbMeleeSkill.SelectedItem) - 1, CInt(cbSTR.SelectedItem), 1, wpnQualities, wpn2, avgtroop)
            atksec = GenerateMeleeAttack(CInt(txtMeleeSkill.Text), CInt(txtStrength.Text), 1, wpnQualities, wpn2, avgtroop)

        End If

        offhandbonus = 1
        If cbMeleeWpnsOff.SelectedItem = "<None>" Then
            offhandbonus = 0
            atkarray.name = atk.name
            atkarray.type = "melee"
            atkarray.pointValue = atk.pointValue
            atkarray.attacks.Add(atk)
            offense.attackArrays.Add(atkarray)
        ElseIf radOffHandBonus.Checked Then
            atkarray.name = "Combined Strike"
            atkarray.type = "melee"
            atkarray.pointValue = atkWBonus.pointValue
            atkarray.attacks.Add(atkWBonus)
            offense.attackArrays.Add(atkarray)
        ElseIf radOffHandSec.Checked Then
            atkarray.name = "Two-Weapon Attack"
            atkarray.type = "melee"
            atkarray.pointValue = atk.pointValue + atksec.pointValue
            atkarray.attacks.Add(atk)
            atkarray.attacks.Add(atksec)
            offense.attackArrays.Add(atkarray)
        Else 'radOffHandBoth must be checked
            atkarray.name = "Combined Strike"
            atkarray.type = "melee"
            atkarray.pointValue = atkWBonus.pointValue
            atkarray.attacks.Add(atkWBonus)
            offense.attackArrays.Add(atkarray)

            atkarray = New AttackArrayType
            atkarray.attacks = New List(Of AttackType)

            atkarray.name = "Two-Weapon Attack"
            atkarray.type = "melee"
            atkarray.pointValue = atk.pointValue + atksec.pointValue
            atkarray.attacks.Add(atk)
            atkarray.attacks.Add(atksec)
            offense.attackArrays.Add(atkarray)
        End If


        If cbRangedWeapons.SelectedItem <> "<None>" Then
            atkarray = New AttackArrayType
            atkarray.attacks = New List(Of AttackType)
            atkarray.name = "Ranged Attack"
            atkarray.type = "ranged"
            atk = New AttackType

            atk.name = cbRangedWeapons.SelectedItem
            atkarray.name = atk.name
            atkarray.type = "ranged"
            wpnrng = weapons.Find(Function(x) x.name = atk.name)

            'atk = GenerateRangedAttack(CInt(cbRangedSkill.SelectedItem), CInt(cbSTR.SelectedItem), 1, wpnQualities, wpnrng)
            atk = GenerateRangedAttack(CInt(txtRangedSkill.Text), CInt(txtStrength.Text), 1, wpnQualities, wpnrng)

            atkarray.attacks.Add(atk)
            atkarray.pointValue = atk.pointValue

            offense.attackArrays.Add(atkarray)
        End If

        Dim offensePVarray As Double()
        offensePVarray = PriceOffense(offense)
        'offense.pointValue = PriceOffense(offense)
        offense.pointValueMelee = offensePVarray(0)
        offense.pointValueRanged = offensePVarray(1)

        'defense = PriceDefense(CInt(cbMeleeSkill.SelectedItem), chkShield.Checked, CInt(cbArmor.SelectedIndex) + 2, CInt(cbWounds.SelectedItem), wpn, offhandbonus, selectedTroopQualities)

        Dim defenseArray As Double()
        'defenseArray = PriceDefense(CInt(txtMeleeSkill.Text), chkShield.Checked, CInt(cbArmor.SelectedIndex) + 2, CInt(txtWounds.Text), wpn.parry, offhandbonus, cbSize.Text, selectedTroopQualities)
        defenseArray = PriceDefense(CInt(txtMeleeSkill.Text), chkShield.Checked, toWoundFirst, toWoundMore, CInt(txtWounds.Text), wpn.parry, offhandbonus, cbSize.Text, selectedTroopQualities)
        defense = defenseArray(0)
        defenseWndsPerTurnMelee = defenseArray(1)
        defenseWndsPerTurnRanged = defenseArray(2)

    End Sub

    Public Sub RefreshCalculations()
        setToWoundValues(cbArmor.SelectedIndex + 2, cbSize.SelectedItem)

        If CustomAttacks = False Then
            GenerateBasicAttackArrays()
        Else
            Dim defenseArray As Double()
            'defenseArray = PriceDefense(CInt(txtMeleeSkill.Text), chkShield.Checked, CInt(cbArmor.SelectedIndex) + 2, CInt(txtWounds.Text), CustomAttackParryMod, 0, cbSize.Text, selectedTroopQualities)
            defenseArray = PriceDefense(CInt(txtMeleeSkill.Text), chkShield.Checked, toWoundFirst, toWoundMore, CInt(txtWounds.Text), CustomAttackParryMod, 0, cbSize.Text, selectedTroopQualities)
            defense = defenseArray(0)
            defenseWndsPerTurnMelee = defenseArray(1)
            defenseWndsPerTurnRanged = defenseArray(2)
        End If
        If chkSpellCaster.Checked Then
            magicCost = PriceMagic(cbCasterLevel.SelectedItem, SpellCostperLevel, cbSpellSources.SelectedItem, chkSingleSpell.Checked)
        Else
            magicCost = 0
        End If
        CalculatePointValue()

    End Sub

    Public Function FormatAttacks(off As OffenseType, Optional inset As Boolean = False) As String
        Dim txt As String = ""
        Dim indent As String = ""
        Dim first As Boolean
        For Each atkarray As AttackArrayType In off.attackArrays
            If inset Then
                indent = vbTab
            Else
                indent = ""
            End If

            first = True
            If atkarray.attacks.Count > 1 Then
                txt += indent & atkarray.name + vbCrLf
                indent += vbTab
                first = False
            End If
            For Each atk As AttackType In atkarray.attacks
                txt += indent
                If first Then
                    txt += atkarray.name
                    first = False
                Else
                    txt += atk.name
                End If
                txt += "  (To-hit: "
                If atk.qualities.Exists(Function(x) x.ToLower = "automatically hits") Then
                    txt += "n/a"
                ElseIf atk.toHit >= 0 Then
                    txt += "+" + atk.toHit.ToString
                Else
                    txt += atk.toHit.ToString
                End If

                If atk.qualities.Exists(Function(x) x.ToLower = "no damage") Then
                    txt += ", Dmg: none"
                Else
                    txt += ", Dmg: d" + atk.dmgDie.ToString
                    If atk.dmgMod > 0 Then
                        txt += "+" + atk.dmgMod.ToString
                    ElseIf atk.dmgMod < 0 Then
                        txt += atk.dmgMod.ToString
                    End If
                End If

                txt += ", "
                If atk.range > 0 Then
                    txt += "Range: " + atk.range.ToString + "/" + (atk.range * 2).ToString + "/" + (atk.range * 3).ToString + ", "
                End If
                txt += "#Atk: "

                If atk.qualities.Exists(Function(x) x.ToLower.Contains("number of attacks equals")) Then
                    txt += "Special)" + vbCrLf
                Else
                    txt += atk.numAttacks.ToString() + ")" + vbCrLf
                End If

                If atk.qualities.Count > 0 Then
                    txt += indent + vbTab + "Qualities: "
                    For Each qual As String In atk.qualities
                        txt += qual
                        If qual <> atk.qualities.Last Then
                            txt += ", "
                        End If
                    Next
                    txt += vbCrLf
                End If
            Next
            'txt += vbCrLf
        Next

        'txtAttacks.Text = txt
        FormatAttacks = txt
    End Function
    Public Sub CalculatePointValue()
        Dim pv, rawPV, healValue, moveMod, qualmod As Double
        Dim Move As Double
        'Dim lowValues = New Double() {0, 2.5, 3.5, 4.5, 5.5, 6.5, 7, 8, 8.5, 9.5}
        'Move = CInt(cbMove.SelectedItem)
        Move = CDbl(txtMove.Text)

        If selectedTroopQualities.Exists(Function(x) x.name.ToLower = "can't run") Then
            Move = Move / 2
        End If

        moveMod = 1 + ((Move - 4) * 0.125)

        qualmod = 1
        For Each qual As QualityType In selectedTroopQualities
            qualmod = qualmod * qual.cost
        Next

        ' 0.4 is the expected wounds per turn with ranged defense 5 and 4 armor.  For ranged attacks, modify them based on the difference between this number and theirs
        Dim rangedDefenseMod As Double = 0.45 / defenseWndsPerTurnRanged
        Dim rangedWoundsMod As Double = Math.Max(CInt(txtWounds.Text) / 2, 1)

        'pv = (offense.pointValue + magicCost) * defense * moveMod * qualmod * 5
        pv = (offense.pointValueMelee + magicCost) * defense
        pv = pv + (offense.pointValueRanged * rangedWoundsMod * rangedDefenseMod)
        pv = pv * moveMod * qualmod

        rawPV = pv

        pv = pv * 6

        pv = Math.Max(1, Math.Round(pv))

        'If pv < 15 Then
        '    ' For low numbers, use point value = 15 - (15-pv) ^ .8 (check values with www.desmos.com/calculator)
        '    'pv = pv * 2
        '    'pv = lowValues(pv)
        '    pv = 15 - pv
        '    pv = 15 - Math.Pow(pv, 0.8)
        '    pv = Math.Round(pv * 4) / 4
        'ElseIf pv < 40 Then
        '    pv = pv - 15
        '    pv = Math.Pow(pv, 0.7) + 15
        '    pv = Math.Round(pv * 2) / 2
        'Else
        '    pv = pv - 15
        '    pv = Math.Pow(pv, 0.7) + 15
        '    pv = Math.Round(pv)
        'End If

        If pv < 10 Then
            ' For low numbers, use point value = 10 - (10-pv) ^ .8 (check values with www.desmos.com/calculator)
            'pv = pv * 2
            'pv = lowValues(pv)
            pv = 10 - pv
            pv = 10 - Math.Pow(pv, 0.8)
            pv = Math.Round(pv * 4) / 4
        ElseIf pv < 40 Then
            pv = pv - 10
            pv = Math.Pow(pv, 0.65) + 10
            pv = Math.Round(pv * 2) / 2
        Else
            pv = pv - 10
            pv = Math.Pow(pv, 0.65) + 10
            pv = Math.Round(pv)
        End If

        lblPointValue.Text = pv

        healValue = Math.Round(rawPV / txtWounds.Text * 2) / 2
        If healValue > 6 Or txtWounds.Text = 1 Or (selectedTroopQualities.Exists(Function(x) x.name.ToLower = "dispersed")) Then
            lblHealCost.Text = "n/a"
        ElseIf healValue = 0 Then
            lblHealCost.Text = "0.5"
        Else
            lblHealCost.Text = Math.Round(rawPV / txtWounds.Text * 2) / 2
        End If

        txtAttacks.Text = FormatAttacks(offense)


    End Sub
    Public Sub ApplyTroopQualitySelect(SelectedQualities As List(Of QualityType))

    End Sub
    Public Function LaunchQualitySelectDlg(AllQualities As List(Of QualityType), SelectedQualities As List(Of QualityType), ByRef Cancelled As Boolean) As List(Of String)
        Dim dlg As New dlgQualityPicker
        LaunchQualitySelectDlg = New List(Of String)

        For Each qual As QualityType In AllQualities
            dlg.lstAllQualities.Items.Add(qual.name)
        Next
        For Each qual As QualityType In SelectedQualities
            dlg.lstSelectedQualities.Items.Add(qual.name)
            dlg.lstAllQualities.Items.Remove(qual.name)
        Next

        Dim result As DialogResult = dlg.ShowDialog(Me)

        If result = Windows.Forms.DialogResult.OK Then
            Dim qualcol As ListBox.ObjectCollection = dlg.SelectedQualities
            For Each qualname As String In qualcol
                LaunchQualitySelectDlg.Add(qualname)
            Next
        ElseIf result = Windows.Forms.DialogResult.Cancel Then
            Cancelled = True
        End If
    End Function
    Public Function LaunchStringSelectDlg(AllStrings As List(Of String), SelectedStrings As List(Of String), ByRef Cancelled As Boolean) As List(Of String)
        Dim dlg As New dlgQualityPicker
        dlg.Text = "Choose Factions"
        LaunchStringSelectDlg = New List(Of String)

        For Each str As String In AllStrings
            dlg.lstAllQualities.Items.Add(str)
        Next
        For Each str As String In SelectedStrings
            dlg.lstSelectedQualities.Items.Add(str)
            dlg.lstAllQualities.Items.Remove(str)
        Next

        Dim result As DialogResult = dlg.ShowDialog(Me)

        If result = Windows.Forms.DialogResult.OK Then
            Dim stringCollection As ListBox.ObjectCollection = dlg.SelectedQualities
            For Each str As String In stringCollection
                LaunchStringSelectDlg.Add(str)
            Next
        ElseIf result = Windows.Forms.DialogResult.Cancel Then
            Cancelled = True
        End If
    End Function

    Private Sub btnQualities_Click(sender As Object, e As EventArgs) Handles btnQualities.Click
        Dim selectedQualities As List(Of String)
        Dim cancelled As Boolean = False
        Dim qual As QualityType = New QualityType
        selectedQualities = New List(Of String)

        selectedQualities = LaunchQualitySelectDlg(troopQualities, selectedTroopQualities, cancelled)

        If cancelled Then
            Exit Sub
        End If

        txtTroopQualities.Text = ""
        selectedTroopQualities.Clear()
        For Each qualstr In selectedQualities
            qual = troopQualities.Find(Function(x) x.name.ToLower = qualstr.ToLower)
            selectedTroopQualities.Add(qual)
            txtTroopQualities.Text += qualstr
            If selectedQualities.Last <> qualstr Then
                txtTroopQualities.Text += "; "
            End If
        Next
        RefreshCalculations()
    End Sub

    Private Sub btnFactions_Click(sender As Object, e As EventArgs) Handles btnFactions.Click
        Dim selFactions As List(Of String)
        Dim cancelled As Boolean = False
        Dim fac As String = ""
        selFactions = New List(Of String)

        selFactions = LaunchStringSelectDlg(factions, selectedFactions, cancelled)

        If cancelled Then
            Exit Sub
        End If

        txtFactions.Text = ""
        selectedFactions.Clear()
        For Each fac In selFactions
            selectedFactions.Add(fac)
            txtFactions.Text += fac
            If selFactions.Last <> fac Then
                txtFactions.Text += "; "
            End If
        Next
    End Sub

    Private Sub chkArmorName_CheckedChanged(sender As Object, e As EventArgs) Handles chkArmorName.CheckedChanged
        If chkArmorName.Checked = False Then
            txtArmorName.Text = ""
            txtArmorName.Visible = False
        Else
            Dim str As String = cbArmor.SelectedItem.ToString
            txtArmorName.Text = str.Substring(3, str.Length - 4)
            txtArmorName.Visible = True
        End If

    End Sub

    Private Sub btnMoveDown_Click(sender As Object, e As EventArgs) Handles btnMoveDn.Click, btnMoveUp.Click, btnStrengthDn.Click, btnStrengthUp.Click, _
        btnMeleeSkillDn.Click, btnMeleeSkillUp.Click, btnRangedSkillUp.Click, btnRangedSkillDn.Click, btnWoundsDn.Click, btnWoundsUp.Click

        Dim StatTextBox As TextBox
        Dim ClickedButton As Button = sender
        Dim statname As String = ClickedButton.Name.Substring(3, ClickedButton.Name.Length - 5)
        Dim upOrDown As Integer = 0
        'Dim statBoxName As String = "txt" + statname

        If ClickedButton.Name.Substring(ClickedButton.Name.Length - 2, 2).ToLower = "up" Then
            upOrDown = 1
        ElseIf ClickedButton.Name.Substring(ClickedButton.Name.Length - 2, 2).ToLower = "dn" Then
            upOrDown = -1
        Else
            upOrDown = 0
        End If

        Select Case statname.ToLower
            Case "move"
                StatTextBox = txtMove
            Case "strength"
                StatTextBox = txtStrength
            Case "meleeskill"
                StatTextBox = txtMeleeSkill
            Case "rangedskill"
                StatTextBox = txtRangedSkill
            Case "wounds"
                StatTextBox = txtWounds
            Case Else
                Exit Sub
        End Select

        adjustStat(StatTextBox, statname, upOrDown)
    End Sub

    Private Sub radOffHandBonus_CheckedChanged(sender As Object, e As EventArgs) Handles radOffHandBonus.CheckedChanged, radOffHandSec.CheckedChanged, radOffhandBoth.CheckedChanged
        If Me.IsHandleCreated = False Then
            Exit Sub
        End If
        RefreshCalculations()
    End Sub

    Private Sub setToWoundValues(armor As Integer, size As String)
        toWoundFirst = armor
        toWoundMore = 4
        Select Case size.ToLower
            Case "small"
                toWoundFirst -= 1
                toWoundMore -= 1
            Case "medium"
                'stays at current values
            Case "cavalry"
                toWoundMore += 1
            Case "large"
                toWoundFirst += 1
                toWoundMore += 1
            Case "huge"
                toWoundFirst += 2
                toWoundMore += 2
        End Select

        Dim tmod As ModifierType
        For Each qual As QualityType In selectedTroopQualities
            tmod = qual.modifiers.Find(Function(x) x.name.ToLower = "armor")
            toWoundFirst += tmod.value
        Next

    End Sub
    Private Sub cbArmor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbArmor.SelectedIndexChanged
        If Me.IsHandleCreated = False Or Initializing = True Then
            Exit Sub
        End If

        RefreshCalculations()
    End Sub
    Private Sub cbSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSize.SelectedIndexChanged
        If Me.IsHandleCreated = False Or Initializing = True Then
            Exit Sub
        End If
        RefreshCalculations()
    End Sub
    Public Sub RevertFormToBasicAttacks()
        CustomAttacks = False

        btnBasicAttacks.Enabled = True
        cbMeleeWpnsOff.Enabled = True
        cbMeleeWpnsPri.Enabled = True
        cbRangedWeapons.Enabled = True
        txtMeleeOffDesc.Enabled = True
        txtMeleePriDesc.Enabled = True
        txtRangedDesc.Enabled = True
        lblOffHandWpns.Enabled = True
        radOffhandBoth.Enabled = True
        radOffHandSec.Enabled = True
        radOffHandBonus.Enabled = True


    End Sub
    Private Sub btnBasicAttacks_Click(sender As Object, e As EventArgs) Handles btnBasicAttacks.Click
        'cbMeleeWpnsOff.SelectedItem = "<None>"
        'cbRangedWeapons.SelectedItem = "<None>"
        'cbMeleeWpnsPri.SelectedItem = "Sword"

        RevertFormToBasicAttacks()

        GenerateBasicAttackArrays()
        RefreshCalculations()

    End Sub
    Public Sub ConvertFormToCustomAttacks()
        btnBasicAttacks.Enabled = True
        cbMeleeWpnsOff.Enabled = False
        cbMeleeWpnsPri.Enabled = False
        cbRangedWeapons.Enabled = False
        txtMeleeOffDesc.Enabled = False
        txtMeleePriDesc.Enabled = False
        txtRangedDesc.Enabled = False
        lblOffHandWpns.Enabled = False
        radOffhandBoth.Enabled = False
        radOffHandSec.Enabled = False
        radOffHandBonus.Enabled = False
    End Sub

    Private Sub btnCustomAttacks_Click(sender As Object, e As EventArgs) Handles btnCustomAttacks.Click
        If CustomAttacks = False Then
            Dim result As MsgBoxResult
            result = MessageBox.Show("Once set to Advanced mode, changes to attributes will no longer automatically update attacks." + vbCrLf + vbCrLf + _
                                     "You can always switch back by pressing the Revert to Basic button" + vbCrLf + vbCrLf + _
                                     "Do you wish to continue?", "Advanced Mode", MessageBoxButtons.YesNo)
            If result = MsgBoxResult.No Then Exit Sub
        End If
        CustomAttacks = True

        ConvertFormToCustomAttacks()
        dlgAdvancedAttacks.Show(Me)

    End Sub


    Public teststr As String

    Private Sub CheckForUpdates()

        Dim DirPath As String
        Dim newSaCDataVersion, newGlobalTroopListVersion As Double
        Dim SaCDataUpdated As Boolean = False
        Dim GlobalTroopListUpdated As Boolean = False
        Dim dlg As New dlgWorking
        dlg.Text = "Checking for updates"
        dlg.Label.Text = "Connecting to server..."
        dlg.Show()
        dlg.Refresh()

        DirPath =
           My.Computer.FileSystem.SpecialDirectories.Temp

        My.Computer.Network.DownloadFile(
           "http://www.riversidegames.us/sac/SaCGenData.xml",
           DirPath &
           "\SaCGenData.xml", "", "", True, 10, True)
        newSaCDataVersion = XMLFileVersion(DirPath + "\SaCGenData.xml")

        My.Computer.Network.DownloadFile(
           "http://www.riversidegames.us/sac/GlobalTroopList.xml",
           DirPath &
           "\GlobalTroopList.xml", "", "", True, 10, True)
        newGlobalTroopListVersion = XMLFileVersion(DirPath + "\GlobalTroopList.xml")

        dlg.Label.Text = "Calculating file versions..."
        dlg.Label.Refresh()

        If newSaCDataVersion > SaCDataVersion Then
            My.Computer.FileSystem.CopyFile(DirPath + "\SaCGenData.xml", AppDirectory + "\SaCGenData.xml", True)
            SaCDataUpdated = True
            SaCDataVersion = newSaCDataVersion
        End If
        If newGlobalTroopListVersion > GlobalTroopListVersion Then
            My.Computer.FileSystem.CopyFile(DirPath + "\GlobalTroopList.xml", AppDirectory + "\GlobalTroopList.xml", True)
            GlobalTroopListUpdated = True
            GlobalTroopListVersion = newGlobalTroopListVersion
        End If

        dlg.Close()
        If GlobalTroopListUpdated = False And SaCDataUpdated = False Then
            MessageBox.Show("No new files found")
        End If

        If GlobalTroopListUpdated = True Then
            MessageBox.Show("A new Global Troop List was downloaded.")
        End If

        If SaCDataUpdated = True Then
            MessageBox.Show("A new settings file was downloaded.  Please close this program and relaunch it for the new settings to be loaded.")
        End If
    End Sub

    Private Sub CheckForUpdatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CheckForUpdatesToolStripMenuItem.Click
        CheckForUpdates()


    End Sub

    Private Sub MenuOpenTroopList_Click(sender As Object, e As EventArgs) Handles menuOpenTroopList.Click, ToolStripButtonOpenTroopList.Click

        'MessageBox.Show(My.Settings.LastUserTroopLocation)
        Dim xmldoc As XmlDocument = New XmlDocument
        Dim xmlroot As XmlNode

        Dim dlg As New OpenFileDialog()
        dlg.Filter = "Troop List files (*.xml)|*.xml"
        dlg.InitialDirectory = My.Settings.LastUserTroopLocation

        Dim result As DialogResult
        result = dlg.ShowDialog

        If result = Windows.Forms.DialogResult.Cancel Then Exit Sub

        Dim working As New dlgWorking
        working.Label.Text = "Opening troop list..."
        working.Show()
        working.Refresh()


        'MessageBox.Show(dlg.FileName)
        'MessageBox.Show(dlg.FileName.Substring(0, dlg.FileName.Length - dlg.SafeFileName.Length))

        xmldoc.Load(dlg.FileName)
        xmlroot = xmldoc.DocumentElement
        Dim listtype As XmlNode = xmlroot.SelectSingleNode("type")

        If IsNothing(listtype) Then
            working.Close()
            MessageBox.Show("Error opening " & dlg.SafeFileName & ".  This does not seem to be a valid troop list file.")
            Exit Sub
        End If

        'I don't care if it is User vs Global anymore
        'If listtype.InnerText <> "User" Then
        'working.Close()
        'MessageBox.Show("Error opening " & dlg.SafeFileName & ".  This does not seem to be a valid user-defined troop list file.")
        'Exit Sub
        'End If

        UserTroopXMLDoc.Load(dlg.FileName)
        UserTroopXMLroot = UserTroopXMLDoc.DocumentElement
        UserTroopFileName = dlg.FileName

        My.Settings.LastUserTroopLocation = dlg.FileName.Substring(0, dlg.FileName.Length - dlg.SafeFileName.Length)
        My.Settings.Save()

        menuLoadFromTroopList.Enabled = True
        ToolStripButtonLoadFromTroopList.Enabled = True
        menuRecalculateAllInTroopList.Enabled = True
        menuSaveCurrent.Enabled = True
        ToolStripButtonSaveCurrentTroop.Enabled = True
        menuPrintArmyList.Enabled = True

        Me.Text = "Sword and Claw Troop Generator - " & dlg.SafeFileName
        menuSaveCurrent.Text = "Save Current Troop in " & dlg.SafeFileName
        menuLoadFromTroopList.Text = "Manage / Load From " & dlg.SafeFileName
        menuRecalculateAllInTroopList.Text = "Recalculate All in " & dlg.SafeFileName
        menuPrintArmyList.Text = dlg.SafeFileName

        working.Close()
        Dim dlgTroop As New dlgTroopList
        dlgTroop.ShowDialog()
    End Sub



    Private Sub menuSaveCurrent_Click(sender As Object, e As EventArgs) Handles menuSaveCurrent.Click, ToolStripButtonSaveCurrentTroop.Click
        Dim overwrite As Boolean = False
        Dim oldnode As XmlNode
        Dim name As String
        Dim testtroops As XmlNodeList = UserTroopXMLroot.SelectNodes("//troop_list/troop")
        For Each testtroop As XmlNode In testtroops
            name = testtroop.SelectSingleNode("name").InnerText
            If name.ToLower = txtTroopName.Text.ToLower Then
                Dim result As DialogResult = MessageBox.Show("A troop with that name already exists.  Do you wish to overwrite?", "", MessageBoxButtons.YesNo)
                If result = Windows.Forms.DialogResult.No Then Exit Sub
                overwrite = True
                oldnode = testtroop
                Exit For
            End If
        Next

        Dim working As New dlgWorking
        working.Label.Text = "Saving " & txtTroopName.Text & "..."
        working.Show()
        working.Refresh()

        Dim troopdoc As XmlDocument = TroopToXML()
        Dim trooproot As XmlNode = troopdoc.DocumentElement

        Dim newtroop As XmlElement = UserTroopXMLDoc.CreateElement("troop")
        newtroop.InnerXml = trooproot.InnerXml

        If overwrite = True Then
            UserTroopXMLroot.ReplaceChild(newtroop, oldnode)
        Else
            UserTroopXMLroot.AppendChild(newtroop)
        End If

        UserTroopXMLDoc.Save(UserTroopFileName)

        working.Close()

    End Sub


    Private Sub menuNewTroopList_Click(sender As Object, e As EventArgs) Handles menuNewTroopList.Click, ToolStripButtonNewTroopList.Click
        Dim dlg As New SaveFileDialog()
        dlg.Filter = "Troop List files (*.xml)|*.xml"
        dlg.AddExtension = True
        dlg.DefaultExt = "xml"
        dlg.InitialDirectory = My.Settings.LastUserTroopLocation

        Dim result As DialogResult
        result = dlg.ShowDialog

        If result = Windows.Forms.DialogResult.Cancel Then Exit Sub


        Dim newfile As System.IO.FileInfo = New System.IO.FileInfo(dlg.FileName)

        Dim working As New dlgWorking
        working.Label.Text = "Creating and opening " & newfile.Name & "..."
        working.Show()
        working.Refresh()

        'My.Settings.LastUserTroopLocation = dlg.FileName.Substring(0, dlg.FileName.Length - dlg.SafeFileName.Length)
        My.Settings.LastUserTroopLocation = newfile.DirectoryName
        My.Settings.Save()

        Dim xmldoc As XmlDocument = New XmlDocument
        Dim xmldecl As XmlDeclaration = xmldoc.CreateXmlDeclaration("1.0", "", "")
        xmldoc.AppendChild(xmldecl)
        Dim xmlroot As XmlElement = xmldoc.CreateElement("troop_list")
        Dim xmltype As XmlElement = xmldoc.CreateElement("type")
        xmltype.InnerText = "user"
        xmlroot.AppendChild(xmltype)
        xmldoc.AppendChild(xmlroot)
        xmldoc.Save(dlg.FileName)

        UserTroopXMLDoc = New XmlDocument
        UserTroopXMLDoc.Load(dlg.FileName)
        UserTroopXMLroot = UserTroopXMLDoc.DocumentElement
        UserTroopFileName = dlg.FileName

        menuLoadFromTroopList.Enabled = True
        menuRecalculateAllInTroopList.Enabled = True
        menuSaveCurrent.Enabled = True
        menuPrintArmyList.Enabled = True

        Me.Text = "Sword and Claw Troop Generator - " & newfile.Name
        menuSaveCurrent.Text = "Save Current Troop in " & newfile.Name
        menuLoadFromTroopList.Text = "Manage / Load From " & newfile.Name
        menuRecalculateAllInTroopList.Text = "Recalculate All in " & newfile.Name
        menuPrintArmyList.Text = newfile.Name

        working.Close()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub menuLoadFromTroopList_Click(sender As Object, e As EventArgs) Handles menuLoadFromTroopList.Click, ToolStripButtonLoadFromTroopList.Click
        Dim dlg As New dlgTroopList
        dlg.ShowDialog()
    End Sub

    Private Sub menuLoadFromGlobalTroopList_Click(sender As Object, e As EventArgs) Handles menuLoadFromGlobalTroopList.Click, ToolStripButtonLoadFromGlobalTroopList.Click
        Dim dlg As New dlgTroopList
        dlg.UseGlobalList = True
        dlg.ShowDialog()
    End Sub
    Private Sub menuRecalculateAllInTroopList_Click(sender As Object, e As EventArgs) Handles menuRecalculateAllInTroopList.Click
        'xmlsrcdoc = frmMain.UserTroopXMLDoc
        'xmlsrcroot = frmMain.UserTroopXMLroot
        Dim working As New dlgWorking
        Dim trpName As String
        Dim troopdoc As XmlDocument
        Dim trooproot As XmlNode
        Dim newtroop As XmlElement

        working.Label.Text = "Loading troop settings..."
        working.Show()
        working.Refresh()


        Dim nodelist As XmlNodeList = UserTroopXMLDoc.SelectNodes("//troop_list/troop")
        For Each troopnode In nodelist
            trpName = troopnode.SelectSingleNode("name").InnerText
            working.Label.Text = "Recalculating " + trpName
            working.Refresh()
            dlgTroopList.LoadTroop(troopnode)


            troopdoc = TroopToXML()
            trooproot = troopdoc.DocumentElement
            newtroop = UserTroopXMLDoc.CreateElement("troop")
            newtroop.InnerXml = trooproot.InnerXml

            UserTroopXMLroot.ReplaceChild(newtroop, troopnode)
            UserTroopXMLDoc.Save(UserTroopFileName)

        Next

        working.Close()

    End Sub
    Public Sub RecalculateOffense()
        Dim updated_atk As AttackType
        Dim wpn As Weapon
        Dim qualList As List(Of QualityType)
        Dim qual As QualityType
        Dim new_offense As OffenseType = New OffenseType
        new_offense.attackArrays = New List(Of AttackArrayType)
        Dim new_atkArray As AttackArrayType

        For Each atckArray As AttackArrayType In offense.attackArrays
            new_atkArray = New AttackArrayType
            new_atkArray.name = atckArray.name
            new_atkArray.type = atckArray.type
            new_atkArray.attacks = New List(Of AttackType)

            For Each atk As AttackType In atckArray.attacks
                wpn = New Weapon
                wpn.dmgDie = atk.dmgDie
                wpn.name = atk.name
                wpn.qualities = atk.qualities
                wpn.range = atk.range
                wpn.strMod = atk.dmgMod
                wpn.type = atk.type

                qualList = New List(Of QualityType)
                For Each qualstr As String In atk.qualities
                    qual = wpnQualities.Find(Function(x) x.name.ToLower = qualstr.ToLower)
                    qualList.Add(qual)
                Next

                If atk.type.ToLower = "melee" Then
                    updated_atk = GenerateMeleeAttack(atk.toHit, 0, atk.numAttacks, qualList, wpn, avgtroop)
                    'updated_atk = GenerateMeleeAttack(atk.toHit, atk.dmgMod, atk.numAttacks, qualList, wpn, avgtroop)
                Else 'Assume it's ranged
                    updated_atk = GenerateRangedAttack(atk.toHit, 0, atk.numAttacks, qualList, wpn)
                End If
                new_atkArray.attacks.Add(updated_atk)
            Next
            new_atkArray.pointValue = PriceAttackArray(new_atkArray)
            new_offense.attackArrays.Add(new_atkArray)
        Next
        offense = new_offense

        Dim offensePVarray As Double()
        offensePVarray = PriceOffense(offense)
        offense.pointValueMelee = offensePVarray(0)
        offense.pointValueRanged = offensePVarray(1)
        PriceOffense(offense)
    End Sub
    Private Sub menuPrintCurrentTroop_Click(sender As Object, e As EventArgs) Handles menuPrintCurrentTroop.Click
        Dim troopdoc As XmlDocument = TroopToXML()
        Dim trooproot As XmlNode = troopdoc.DocumentElement



        Dim xmldoc As XmlDocument = New XmlDocument
        Dim xmldecl As XmlDeclaration = xmldoc.CreateXmlDeclaration("1.0", "", "")
        xmldoc.AppendChild(xmldecl)
        Dim xmlrootElement As XmlElement = xmldoc.CreateElement("troop_list")
        xmldoc.AppendChild(xmlrootElement)
        Dim xmlroot As XmlNode = xmldoc.DocumentElement
        Dim newtroop As XmlElement = xmldoc.CreateElement("troop")
        newtroop.InnerXml = trooproot.InnerXml
        xmlroot.AppendChild(newtroop)
        PrintTroopList(xmlroot)


    End Sub

    Private Sub MenuPrintArmyList_Click(sender As Object, e As EventArgs) Handles menuPrintArmyList.Click
        PrintTroopList(UserTroopXMLroot)
    End Sub

    Private Sub MenuPrintGlobalTroopList_Click(sender As Object, e As EventArgs) Handles menuPrintGlobalTroopList.Click
        Dim xmldoc = New XmlDocument
        Dim xmlroot As XmlNode

        xmldoc.Load(GlobalTroopListFile)
        xmlroot = xmldoc.DocumentElement

        PrintTroopList(xmlroot)

    End Sub
    Public Function SortTroopListByName(tList As List(Of TroopType)) As TroopType()
        'SortTroopListByName = New List(Of TroopType)
        Dim lowest As TroopType = New TroopType
        Dim troop As TroopType = New TroopType
        Dim sorted(tList.Count - 1) As TroopType

        For i = 0 To tList.Count - 1
            lowest = tList.First
            For j = 0 To tList.Count - 1
                troop = tList(j)
                If troop.name < lowest.name Then
                    lowest = troop
                End If
            Next
            tList.Remove(lowest)
            sorted(i) = lowest
        Next
        SortTroopListByName = sorted

    End Function
    Public Function SortTroopListByPV(tList As List(Of TroopType)) As TroopType()
        Dim lowest As TroopType = New TroopType
        Dim troop As TroopType = New TroopType
        Dim sorted(tList.Count - 1) As TroopType

        For i = 0 To tList.Count - 1
            lowest = tList.First
            For j = 0 To tList.Count - 1
                troop = tList(j)
                If CDbl(troop.pointValue) < CDbl(lowest.pointValue) Then
                    lowest = troop
                End If
            Next
            tList.Remove(lowest)
            sorted(i) = lowest
        Next
        SortTroopListByPV = sorted
    End Function
    Public Function CalculateBasicPointValuePerWound(PV As Double, wounds As Integer) As Double
        CalculateBasicPointValuePerWound = 0
        'CalculateBasicPointValuePerWound = (Math.Pow((PV - 10), (1 / 0.7)) + 10) / wounds / 5
        CalculateBasicPointValuePerWound = (Math.Pow((PV - 15), (1 / 0.65)) + 15) / wounds / 5
        'MessageBox.Show(PV & ";" & wounds & ";" & CalculateBasicPointValuePerWound.ToString)
    End Function
    Public Sub PrintTroopList(alltroops As XmlNode)

        Dim dlg As New SaveFileDialog()
        dlg.Filter = "Text files (*.txt)|*.txt"
        dlg.AddExtension = True
        dlg.DefaultExt = "txt"
        dlg.InitialDirectory = My.Settings.LastUserTroopLocation

        Dim result As DialogResult
        result = dlg.ShowDialog

        If result = Windows.Forms.DialogResult.Cancel Then Exit Sub

        Dim newfile As System.IO.FileInfo = New System.IO.FileInfo(dlg.FileName)

        Dim working As New dlgWorking
        working.Label.Text = "Creating " & newfile.Name & "..."
        working.Show()
        working.Refresh()

        My.Settings.LastUserTroopLocation = newfile.DirectoryName
        My.Settings.Save()

        'Dim DirPath As String = My.Computer.FileSystem.SpecialDirectories.Temp


        Dim troopnodelist As XmlNodeList = alltroops.SelectNodes("troop")

        Dim troopstr As String
        Dim troop As TroopType = New TroopType
        Dim txt As String = ""
        Dim trooplist As List(Of TroopType) = New List(Of TroopType)


        For Each troopnode As XmlNode In troopnodelist
            troopstr = AddXMLFormatting(troopnode.InnerXml)
            troop = XMLToTroop(AddXMLFormatting(troopnode.InnerXml))
            trooplist.Add(troop)

        Next

        If trooplist.Count = 0 Then
            working.Close()
            MessageBox.Show("Error: no troop definitions found")
            Exit Sub
        End If

        Dim trooparray(trooplist.Count - 1) As TroopType
        If SortByNameToolStripMenuItem.Checked = True Then
            trooparray = SortTroopListByName(trooplist)
        ElseIf SortByPointValueToolStripMenuItem.Checked = True Then
            trooparray = SortTroopListByPV(trooplist)
        End If

        Dim pvperwound As Double = 0
        Dim offValue As Double = 0

        For i = 0 To trooparray.Length - 1
            troop = trooparray(i)

            txt += troop.name & "  (Point Value: " & troop.pointValue & ")" & "  Size: " & troop.size & vbCrLf

            If troop.spellcaster Then
                txt += Space(4) & "Spellcaster, Level " & CInt(troop.spellLevel).ToString
                If troop.spellSingle Then
                    txt += " (Single spell only)"
                End If
                txt += vbCrLf
            End If

            txt += vbTab & "Move: " & troop.move & vbTab & ", Strength: " & troop.str & vbCrLf & vbTab &
                "Defenses:  Melee " & troop.DefenseMelee & ", Ranged " & troop.DefenseRanged & vbCrLf & vbTab &
                "Armor: " & troop.DefenseArmor
            If troop.armorCustom = False Then
                txt += troop.armor.Substring(2)
            Else
                txt += "(" & troop.armorCustomName & ")"
            End If
            If troop.shield = True Then
                txt += " + shield"
            End If
            txt += ",  Wounds: " & troop.wounds

            If troop.qualitiesList.Count > 0 Then
                txt += vbCrLf & vbTab & "Troop Qualities: "
                For Each qual As QualityType In troop.qualitiesList
                    txt += qual.name
                    If qual.name <> troop.qualitiesList.Last.name Then
                        txt += ", "
                    End If
                Next
            End If

            txt += vbCrLf & Space(4) & "Magic: Heal Cost "
            If troop.wounds = 1 Or (troop.qualitiesList.Exists(Function(x) x.name.ToLower = "dispersed")) Then
                txt += "n/a"
            Else
                If troop.pointValue < 15 Then
                    txt += "1/2"
                    ' For low numbers, use point value = 15 - (15-pv) ^ .8 (check values with www.desmos.com/calculator)
                    'PV = 15 - PV()
                    'PV = 15 - Math.Pow(PV, 0.8)
                    pvperwound = (Math.Pow((15 - troop.pointValue), (1 / 0.8)) + 15) / troop.wounds / 5
                    '(Math.Pow((PV - 15), (1 / 0.7)) + 15) / wounds / 5
                Else
                    pvperwound = CalculateBasicPointValuePerWound(troop.pointValue, troop.wounds)
                    'pvperwound = (Math.Pow((troop.pointValue - 10), (1 / 0.7)) + 10) / troop.wounds / 5
                End If

                If pvperwound <= 0.5 Then
                        txt += "1/2"
                    ElseIf pvperwound <= 1 Then
                        txt += "1"
                    ElseIf pvperwound <= 1.5 Then
                        txt += "1.5"
                    ElseIf pvperwound <= 2 Then
                        txt += "2"
                    ElseIf pvperwound <= 2.5 Then
                        txt += "2.5"
                    ElseIf pvperwound <= 3 Then
                        txt += "3"
                    ElseIf pvperwound <= 3.5 Then
                        txt += "3.5"
                    ElseIf pvperwound <= 4 Then
                        txt += "4"
                    ElseIf pvperwound <= 4.5 Then
                        txt += "4.5"
                    ElseIf pvperwound <= 5 Then
                        txt += "5"
                    ElseIf pvperwound <= 5.5 Then
                        txt += "5.5"
                    ElseIf pvperwound <= 6 Then
                        txt += "6"
                    Else
                        txt += "n/a"
                    End If

                End If

            txt += vbCrLf & Space(4) & "Attacks:"
            txt += vbCrLf & FormatAttacks(troop.offense, True) & vbCrLf & vbCrLf
        Next

        Dim outfile As System.IO.StreamWriter
        outfile = My.Computer.FileSystem.OpenTextFileWriter(newfile.FullName, False)
        outfile.Write(txt)
        outfile.Close()

        working.Close()

        System.Diagnostics.Process.Start("notepad.exe", newfile.FullName)

    End Sub

    Private Sub SortByPointValueToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SortByPointValueToolStripMenuItem.Click
        SortByNameToolStripMenuItem.Checked = Not SortByPointValueToolStripMenuItem.Checked
        menuPrintSave.ShowDropDown()
    End Sub

    Private Sub SortByNameToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SortByNameToolStripMenuItem.Click
        SortByPointValueToolStripMenuItem.Checked = Not SortByPointValueToolStripMenuItem.Checked
        menuPrintSave.ShowDropDown()
    End Sub

    Private Sub chkSpellCaster_CheckedChanged(sender As Object, e As EventArgs) Handles chkSpellCaster.CheckedChanged
        If chkSpellCaster.Checked Then

            lblCasterLevel.Visible = True
            lblSpellsKnown.Visible = True
            cbCasterLevel.Visible = True
            cbSpellSources.Visible = True
            chkSingleSpell.Visible = True
            magicCost = PriceMagic(cbCasterLevel.SelectedItem.ToString, SpellCostperLevel, cbSpellSources.SelectedItem.ToString, chkSingleSpell.Checked)
            CalculatePointValue()
        Else
            magicCost = 0
            lblCasterLevel.Visible = False
            lblSpellsKnown.Visible = False
            cbCasterLevel.Visible = False
            cbSpellSources.Visible = False
            chkSingleSpell.Visible = False
            CalculatePointValue()
        End If
    End Sub

    Protected Function SanitizeURLString(ByVal RawURLParameter As String) As String
        REM from https://stackoverflow.com/questions/561954/asp-net-urlencode-ampersand-for-use-in-query-string, used for the call to the troop card to escape ampersands properly.
        Dim Results As String

        Results = RawURLParameter

        Results = Results.Replace("%", "%25")
        Results = Results.Replace("<", "%3C")
        Results = Results.Replace(">", "%3E")
        Results = Results.Replace("#", "%23")
        Results = Results.Replace("{", "%7B")
        Results = Results.Replace("}", "%7D")
        Results = Results.Replace("|", "%7C")
        Results = Results.Replace("\", "%5C")
        Results = Results.Replace("^", "%5E")
        Results = Results.Replace("~", "%7E")
        Results = Results.Replace("[", "%5B")
        Results = Results.Replace("]", "%5D")
        Results = Results.Replace("`", "%60")
        Results = Results.Replace(";", "%3B")
        Results = Results.Replace("/", "%2F")
        Results = Results.Replace("?", "%3F")
        Results = Results.Replace(":", "%3A")
        Results = Results.Replace("@", "%40")
        Results = Results.Replace("=", "%3D")
        Results = Results.Replace("&", "%26")
        Results = Results.Replace("$", "%24")
        Results = Results.Replace(".", "%2E")

        Return Results

    End Function

    Private Sub linkTroopCard_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles linkTroopCard.LinkClicked

        Dim webXML As String
        Dim troopdoc As XmlDocument
        Dim trooproot As XmlNode
        Dim newtroop As XmlElement
        Dim webAddr As String

        troopdoc = TroopToXML()
        trooproot = troopdoc.DocumentElement
        newtroop = UserTroopXMLDoc.CreateElement("troop")
        newtroop.InnerXml = trooproot.InnerXml
        webXML = SanitizeURLString(newtroop.OuterXml)
        webXML = System.Web.HttpUtility.UrlEncode(webXML)

        If webserverLocalTF Then
            webAddr = webserverLocalAddr
        Else
            webAddr = "home.riversidegames.us:8008"
        End If

        System.Diagnostics.Process.Start("http://" + webAddr + "/sac/troopcard.php?remote=true&troopxml=" + webXML)

    End Sub






    'Private Sub menuRecalculateAllInTroopList_Click_1(sender As Object, e As EventArgs) Handles menuRecalculateAllInTroopList.Click

    'End Sub

    Private Sub cbCasterLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCasterLevel.SelectedIndexChanged, cbSpellSources.SelectedIndexChanged, chkSingleSpell.CheckedChanged
        If Not Initializing Then
            magicCost = PriceMagic(cbCasterLevel.SelectedItem.ToString, SpellCostperLevel, cbSpellSources.SelectedItem.ToString, chkSingleSpell.Checked)
            CalculatePointValue()
        End If
    End Sub


End Class

