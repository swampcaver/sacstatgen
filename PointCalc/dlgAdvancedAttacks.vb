﻿Imports System.Windows.Forms

Public Class dlgAdvancedAttacks
    Dim CustomOffense As OffenseType
    Dim IgnoreChanges As Boolean = False

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        If Not IsNumeric(txtParryMod.Text) Then
            MessageBox.Show("The parry modifier must be numeric")
            txtParryMod.Focus()
            Exit Sub
        End If
        frmMain.CustomAttackParryMod = CInt(txtParryMod.Text)
        Dim hasMelee, hasRanged As Boolean
        Dim newarray As AttackArrayType = New AttackArrayType
        For i = 0 To CustomOffense.attackArrays.Count - 1
            hasMelee = False
            hasRanged = False
            newarray = CustomOffense.attackArrays(i)
            For j = 0 To newarray.attacks.Count - 1
                If newarray.attacks(j).type.ToLower = "melee" Then
                    hasMelee = True
                ElseIf newarray.attacks(j).type.ToLower = "ranged" Then
                    hasRanged = True
                End If
            Next
            If hasMelee = True And hasRanged = True Then
                newarray.type = "both"
            ElseIf hasRanged = True Then
                newarray.type = "ranged"
            Else
                newarray.type = "melee"
            End If
            CustomOffense.attackArrays(i) = newarray
        Next

        Dim offensePVarray As Double()
        frmMain.offense = CustomOffense
        frmMain.txtAttacks.Text = frmMain.FormatAttacks(frmMain.offense)
        'frmMain.offense.pointValue = PriceOffense(frmMain.offense)
        offensePVarray = PriceOffense(frmMain.offense)
        frmMain.offense.pointValueMelee = offensePVarray(0)
        frmMain.offense.pointValueRanged = offensePVarray(1)
        'frmMain.CalculatePointValue()
        frmMain.RefreshCalculations()
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.Close()
    End Sub
    Private Sub FillAttackArrayList()
        lstAttackArrays.Items.Clear()
        For Each atkarray In CustomOffense.attackArrays
            lstAttackArrays.Items.Add(atkarray.name)
        Next
    End Sub
    Private Sub FillAttacksList(arrayname As String)
        lstAttacks.Items.Clear()
        Dim atkArray As AttackArrayType = CustomOffense.attackArrays.Find(Function(x) x.name.ToLower = arrayname.ToLower)
        For Each atk In atkArray.attacks
            lstAttacks.Items.Add(atk.name)
        Next
    End Sub
    Private Sub dlgAdvancedAttacks_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CustomOffense = frmMain.offense
        FillAttackArrayList()
        lstAttackArrays.SelectedIndex = 0
        txtParryMod.Text = frmMain.CustomAttackParryMod
    End Sub

    Private Sub lstAttackArrays_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstAttackArrays.SelectedIndexChanged
        If IgnoreChanges = True Then
            Exit Sub
        End If
        FillAttacksList(lstAttackArrays.SelectedItem)
        lstAttacks.SelectedIndex = 0
        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub
    Private Sub btnAttackMove_Click(sender As Object, e As EventArgs) Handles btnAttackMoveUp.Click, btnAttackMoveDn.Click
        Dim btn As Button = sender
        Dim direction As Integer
        If btn.Name.Substring(btn.Name.Length - 2, 2).ToLower = "up" Then
            direction = -1
            If lstAttacks.SelectedIndex = 0 Then
                Exit Sub
            End If
        Else
            direction = 1
            If lstAttacks.SelectedIndex = lstAttacks.Items.Count - 1 Then
                Exit Sub
            End If
        End If
        Dim indx As Integer = lstAttacks.SelectedIndex
        Dim atkName As String = lstAttacks.SelectedItem

        IgnoreChanges = True

        lstAttacks.Items.RemoveAt(indx)
        lstAttacks.Items.Insert(indx + direction, atkName)
        Dim atkarrayindx As Integer = lstAttackArrays.SelectedIndex
        Dim atk As AttackType = CustomOffense.attackArrays.Item(atkarrayindx).attacks.Item(indx)
        CustomOffense.attackArrays.Item(atkarrayindx).attacks.RemoveAt(indx)
        CustomOffense.attackArrays.Item(atkarrayindx).attacks.Insert(indx + direction, atk)


        IgnoreChanges = False
        lstAttacks.SelectedIndex = indx + direction

        'Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Find(Function(x) x.name.ToLower = atkarrayName.ToLower)
        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub
   
    Private Sub btnAtkArrayMove_Click(sender As Object, e As EventArgs) Handles btnAtkArrayMoveUp.Click, btnAtkArrayMoveDn.Click
        Dim btn As Button = sender
        Dim direction As Integer
        If btn.Name.Substring(btn.Name.Length - 2, 2).ToLower = "up" Then
            direction = -1
            If lstAttackArrays.SelectedIndex = 0 Then
                Exit Sub
            End If
        Else
            direction = 1
            If lstAttackArrays.SelectedIndex = lstAttackArrays.Items.Count - 1 Then
                Exit Sub
            End If
        End If
        Dim indx As Integer = lstAttackArrays.SelectedIndex
        Dim atkarrayName As String = lstAttackArrays.SelectedItem

        IgnoreChanges = True

        lstAttackArrays.Items.RemoveAt(indx)
        lstAttackArrays.Items.Insert(indx + direction, atkarrayName)

        Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Item(indx)
        CustomOffense.attackArrays.RemoveAt(indx)
        CustomOffense.attackArrays.Insert(indx + direction, atkarray)


        IgnoreChanges = False
        lstAttackArrays.SelectedIndex = indx + direction

        'Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Find(Function(x) x.name.ToLower = atkarrayName.ToLower)
        
    End Sub

    Private Sub btnDeleteAttackArray_Click(sender As Object, e As EventArgs) Handles btnDeleteAttackArray.Click

        If lstAttackArrays.Items.Count = 1 Then
            MessageBox.Show("You cannot delete the last attack set")
            Exit Sub
        End If
        Dim indx As Integer = lstAttackArrays.SelectedIndex
        Dim atkarrayName As String = lstAttackArrays.SelectedItem

        IgnoreChanges = True

        lstAttackArrays.Items.RemoveAt(indx)
        
        Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Item(indx)
        CustomOffense.attackArrays.RemoveAt(indx)
        

        IgnoreChanges = False
        If indx = 0 Then
            lstAttackArrays.SelectedIndex = 0
        Else
            lstAttackArrays.SelectedIndex = indx - 1
        End If
    End Sub

    Private Sub btnRenameAttackArray_Click(sender As Object, e As EventArgs) Handles btnRenameAttackArray.Click
        Dim indx As Integer = lstAttackArrays.SelectedIndex
        Dim atkarrayName As String = lstAttackArrays.SelectedItem
        Dim newname As String = InputBox("", "Edit name", atkarrayName)

        If newname = "" Or newname = atkarrayName Then Exit Sub

        IgnoreChanges = True

        lstAttackArrays.Items(indx) = newname

        Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Item(indx)
        atkarray.name = newname
        CustomOffense.attackArrays.Item(indx) = atkarray


        lstAttackArrays.SelectedIndex = indx
        IgnoreChanges = False
        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub

    Private Sub btnAttackRename_Click(sender As Object, e As EventArgs) Handles btnAttackRename.Click
        Dim arrayindx As Integer = lstAttackArrays.SelectedIndex
        Dim indx As Integer = lstAttacks.SelectedIndex
        Dim atkName As String = lstAttacks.SelectedItem
        Dim newname As String = InputBox("", "Edit name", atkName)

        If newname = "" Or newname = atkName Then Exit Sub

        IgnoreChanges = True

        lstAttacks.Items(indx) = newname

        Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Item(arrayindx)
        Dim atk As AttackType = CustomOffense.attackArrays(arrayindx).attacks.Item(indx)

        atk.name = newname
        CustomOffense.attackArrays.Item(arrayindx).attacks.Item(indx) = atk


        lstAttacks.SelectedIndex = indx
        IgnoreChanges = False
        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub

    Private Sub btnDeleteAttack_Click(sender As Object, e As EventArgs) Handles btnDeleteAttack.Click
        If lstAttacks.Items.Count = 1 Then
            MessageBox.Show("You cannot delete the last attack")
            Exit Sub
        End If
        Dim arrayindx As Integer = lstAttackArrays.SelectedIndex
        Dim indx As Integer = lstAttacks.SelectedIndex
        Dim atkName As String = lstAttacks.SelectedItem
        
        IgnoreChanges = True

        lstAttacks.Items.RemoveAt(indx)

        Dim atkarray As AttackArrayType = CustomOffense.attackArrays.Item(arrayindx)
        Dim atk As AttackType = CustomOffense.attackArrays.Item(arrayindx).attacks(indx)
        'CustomOffense.attackArrays.Item(arrayindx).attacks.RemoveAt(indx)
        atkarray.attacks.RemoveAt(indx)
        atkarray.pointValue = PriceAttackArray(atkarray)

        CustomOffense.attackArrays.Item(arrayindx) = atkarray
        IgnoreChanges = False
        If indx = 0 Then
            lstAttacks.SelectedIndex = 0
        Else
            lstAttacks.SelectedIndex = indx - 1
        End If

        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub


    Private Sub btnAttackArrayAdd_Click(sender As Object, e As EventArgs) Handles btnAttackArrayAdd.Click

        Dim newname As String = InputBox("Enter a name for the new attack set", "New attack set")
        If newname = "" Then Exit Sub

        Dim newArray As AttackArrayType = New AttackArrayType
        newArray.attacks = New List(Of AttackType)
        newArray.name = newname



        Dim defaultatk As AttackType = New AttackType
        Dim newatk As AttackType = New AttackType
        Dim wpn As Weapon = New Weapon
        wpn = frmMain.weapons.Find(Function(x) x.name.ToLower = "sword")
        defaultatk = GenerateMeleeAttack(CInt(frmMain.txtMeleeSkill.Text), CInt(frmMain.txtStrength.Text), 1, New List(Of QualityType), wpn, avgtroop)

        Dim dlg As New dlgNewAttack
        dlg.txtName.Text = newname
        Dim result As DialogResult = dlg.ShowDialog(Me)

        If result = Windows.Forms.DialogResult.OK Then
            newatk = dlg.propNewAttack
        ElseIf result = Windows.Forms.DialogResult.Cancel Then
            newatk = defaultatk
        End If

        newArray.attacks.Add(newatk)
        newArray.pointValue = PriceAttackArray(newArray)

        CustomOffense.attackArrays.Add(newArray)
        lstAttackArrays.Items.Add(newArray.name)
        lstAttackArrays.SelectedItem = newArray.name


    End Sub

    Private Sub btnAttackAdd_Click(sender As Object, e As EventArgs) Handles btnAttackAdd.Click
        Dim newatk As AttackType = New AttackType
        Dim arrayindx As Integer
        Dim newArray As AttackArrayType

        Dim dlg As New dlgNewAttack
        Dim result As DialogResult = dlg.ShowDialog(Me)

        If result = Windows.Forms.DialogResult.OK Then
            newatk = dlg.propNewAttack
        ElseIf result = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        arrayindx = CustomOffense.attackArrays.FindIndex(Function(x) x.name.ToLower = lstAttackArrays.SelectedItem.ToString.ToLower)
        newArray = CustomOffense.attackArrays(arrayindx)
        newArray.attacks.Add(newatk)
        newArray.pointValue = PriceAttackArray(newArray)
        CustomOffense.attackArrays(arrayindx) = newArray

        lstAttacks.Items.Add(newatk.name)
        lstAttacks.SelectedItem = newatk.name

        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub

    Private Sub btnEditAttack_Click(sender As Object, e As EventArgs) Handles btnEditAttack.Click
        Dim oldAtk As AttackType
        Dim newatk As AttackType = New AttackType
        Dim arrayindx As Integer
        Dim newArray As AttackArrayType
        
        arrayindx = CustomOffense.attackArrays.FindIndex(Function(x) x.name.ToLower = lstAttackArrays.SelectedItem.ToString.ToLower)
        newArray = CustomOffense.attackArrays(arrayindx)

        oldAtk = newArray.attacks.Find(Function(x) x.name.ToLower = lstAttacks.SelectedItem.ToString.ToLower)

        Dim dlg As New dlgNewAttack
        dlg.txtName.Text = oldAtk.name
        dlg.txtDmgMod.Text = oldAtk.dmgMod
        dlg.cbDmgDie.SelectedItem = oldAtk.dmgDie.ToString
        dlg.txtNumAttacks.Text = oldAtk.numAttacks
        dlg.txtToHit.Text = oldAtk.toHit
        If oldAtk.type.ToLower = "ranged" Then
            dlg.cbRanged.Checked = True
            dlg.txtRange.Text = oldAtk.range
        End If
        'For Each qual In oldAtk.qualities
        ' dlg.txtQualities.Text += qual
        'Next
        Dim qual As QualityType = New QualityType
        Dim qualList As List(Of QualityType) = New List(Of QualityType)
        For Each qualstr In oldAtk.qualities
            qual = frmMain.wpnQualities.Find(Function(x) x.name.ToLower = qualstr.ToLower)
            qualList.Add(qual)
            dlg.txtQualities.Text += qualstr
            If oldAtk.qualities.Last <> qualstr Then
                dlg.txtQualities.Text += "; "
            End If
        Next
        dlg.Qualities = qualList
        Dim result As DialogResult = dlg.ShowDialog(Me)

        If result = Windows.Forms.DialogResult.OK Then
            newatk = dlg.propNewAttack
        ElseIf result = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        newArray.attacks.Remove(oldAtk)
        newArray.attacks.Add(newatk)
        newArray.pointValue = PriceAttackArray(newArray)
        CustomOffense.attackArrays(arrayindx) = newArray

        lstAttacks.Items.Remove(oldAtk.name)
        lstAttacks.Items.Add(newatk.name)
        lstAttacks.SelectedItem = newatk.name

        txtCustAttack.Text = frmMain.FormatAttacks(CustomOffense)
    End Sub
End Class
