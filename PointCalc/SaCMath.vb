﻿Module SaCMath
    Public Structure basicOpponent
        Public move As Integer
        Public parry As Integer
        Public wounds As Integer
        'Public armor As Integer
        Public wndFirst As Integer
        Public wndMore As Integer
        Public tohit As Integer
        Public dmgDie As Integer
        Public dmgMod As Integer
        Public shield As Boolean
        Public Sub New(s As String)
            Me.move = 4
            Me.parry = 5
            Me.wounds = 2
            'Me.armor = 4
            Me.wndFirst = 4
            Me.wndMore = 4
            Me.tohit = 0
            Me.dmgDie = 6
            Me.dmgMod = 1
            Me.shield = False
        End Sub
    End Structure
    Public avgtroop As basicOpponent

    Public Function Evaluate(expression As String) As Double
        Dim dt As New DataTable
        Dim result As Object = dt.Compute(expression, "")
        Evaluate = result.ToString
    End Function

    Public Function HitsPerAttack(tohit As Integer, defense As Integer) As Integer
        HitsPerAttack = 0
        For i = 1 To 10
            If i + tohit >= defense And i + tohit < defense + 5 Then
                HitsPerAttack += 1
            End If
        Next
    End Function

    'Public Function WoundsPerHit(dmgDie As Integer, dmgMod As Integer, critLevel As Integer, armor As Integer, maxWounds As Integer) As Double
    Public Function WoundsPerHit(dmgDie As Integer, dmgMod As Integer, critLevel As Integer, wndFirst As Integer, wndMore As Integer, maxWounds As Integer) As Double
        Dim total As Integer = 0
        Dim dmg, critdmg As Integer
        critdmg = critLevel * dmgDie / 2
        For i = (1 + dmgMod + critdmg) To (dmgDie + dmgMod + critdmg)
            If i >= wndFirst Then
                dmg = 1 + Math.Floor((i - wndFirst) / wndMore)
            Else
                dmg = 0
            End If
            total += Math.Min(dmg, maxWounds)
        Next
        WoundsPerHit = total / dmgDie
    End Function
    Public Function GenerateMeleeAttack(MeleeSkill As Integer, str As Integer, numAttacks As Integer, qualities As List(Of QualityType), wpn As Weapon, target As basicOpponent) As AttackType
        Dim atk As AttackType = New AttackType
        Dim strbonus As Integer
        atk.qualities = New List(Of String)

        
        If wpn.qualities.Exists(Function(x) x.ToLower = "no damage") Then
            wpn.dmgDie = 6
            wpn.strMod = 1
            str = 0
        End If


        Dim hits As Integer = 11 - target.parry + MeleeSkill
        'Dim hits As Integer = HitsPerAttack(MeleeSkill, target.parry)
        'Dim crits As Integer = HitsPerAttack(MeleeSkill, target.parry + 5)
        'Dim dblcrits As Integer = HitsPerAttack(MeleeSkill, target.parry + 10)

        If wpn.qualities.Exists(Function(x) x.ToLower = "automatically hits") Then
            hits = 10
            'crits = 0
            'dblcrits = 0
        End If

        If wpn.strMod Is Nothing Then
            strbonus = str
        ElseIf IsNumeric(wpn.strMod) Then
            strbonus = str + CInt(wpn.strMod)
        Else 'assume wpn.strMod is a math equation
            Dim formula As String = Replace(wpn.strMod, "str", CStr(str),,, CompareMethod.Text)
            strbonus = Math.Floor(Evaluate(formula))
        End If

        Dim dmgbase As Double = WoundsPerHit(wpn.dmgDie, strbonus, 0, target.wndFirst, target.wndMore, target.wounds)
        'Dim dmgbase As Double = WoundsPerHit(wpn.dmgDie, str, 0, target.armor, target.wounds)
        'Dim dmgcrit As Double = WoundsPerHit(wpn.dmgDie, str, 1, target.armor, target.wounds)
        'Dim dmgdblcrit As Double = WoundsPerHit(wpn.dmgDie, str, 2, target.armor, target.wounds)

        Dim wnds As Double
        wnds += hits * dmgbase
        ' Modified to get rid of crits
        'wnds += crits * dmgcrit
        'wnds += dblcrits * dmgdblcrit

        ' Remarked out after getting rid of hitsperattack
        'wnds += crits * dmgbase
        'wnds += dblcrits * dmgbase

        Dim avgwnds As Double = wnds / 10
        Dim numAttacksMod As Double = numAttacks
        If wpn.qualities.Exists(Function(x) x.ToLower = "number of attacks equals health") Then
            numAttacksMod = (CInt(frmMain.txtWounds.Text) + 1) / 2
        ElseIf wpn.qualities.Exists(Function(x) x.ToLower = "number of attacks equals half health") Then
            numAttacksMod = (CInt(frmMain.txtWounds.Text) + 2) / 4
        End If
        avgwnds = avgwnds * numAttacksMod

        Dim qualitymod As Single = 1
        For Each qual As String In wpn.qualities
            qualitymod = qualitymod * qualities.Find(Function(x) x.name.ToLower = qual.ToLower).cost
            atk.qualities.Add(qual)
        Next

        atk.pointValue = avgwnds * qualitymod

        atk.name = wpn.name
        atk.type = "Melee"
        atk.toHit = MeleeSkill
        atk.dmgDie = wpn.dmgDie
        atk.dmgMod = strbonus
        atk.numAttacks = numAttacks
        atk.expectedHits = hits
        'atk.expectedHits = hits + crits + dblcrits
        atk.expectedWounds = avgwnds
        GenerateMeleeAttack = atk

    End Function
    Public Function GenerateRangedAttack(RangedSkill As Integer, str As Integer, numAttacks As Integer, qualities As List(Of QualityType), wpn As Weapon) As AttackType

        'Dim strbonus As Integer
        Dim atk As AttackType = New AttackType
        Dim atkMedRange As AttackType = New AttackType
        Dim atkLongRange As AttackType = New AttackType

        'If wpn.strMod Is Nothing Then
        '    If str <= 0 Then
        '        strbonus = str
        '    Else
        '        strbonus = Math.Floor(str / 2)
        '    End If
        'ElseIf IsNumeric(wpn.strMod) Then
        '    strbonus = str + CInt(wpn.strMod)
        'Else 'assume wpn.strMod is a math equation based on str
        '    Dim formula As String = Replace(wpn.strMod, "str", CStr(str),,, CompareMethod.Text)
        '    strbonus = Math.Floor(Evaluate(formula))
        'End If
        Dim rangedTargetTroop As basicOpponent = avgtroop
        rangedTargetTroop.parry = 6

        atkLongRange = GenerateMeleeAttack(RangedSkill - 2, str, 1, qualities, wpn, rangedTargetTroop)
        atkMedRange = GenerateMeleeAttack(RangedSkill - 1, str, 1, qualities, wpn, rangedTargetTroop)
        atk = GenerateMeleeAttack(RangedSkill, str, 1, qualities, wpn, rangedTargetTroop)
        If wpn.qualities.Exists(Function(x) x.ToLower = "minimum range") Then
            atk.pointValue = atk.pointValue / 2
        End If
        atk.type = "ranged"
        atk.range = wpn.range

        'Account for range
        atk.pointValue = (atk.pointValue + atkMedRange.pointValue + atkLongRange.pointValue) * wpn.range / (rangedTargetTroop.move * 2)

        'Account for the firing unit's speed unless they have a slow weapon
        atk.pointValue = atk.pointValue * RangedAttackMoveMod(CInt(frmMain.txtMove.Text), wpn)

        'Assume the ranged attack will target a higher-point unit
        atk.pointValue = atk.pointValue * 3

        GenerateRangedAttack = atk

    End Function
    Public Function RangedAttackMoveMod(move As Integer, wpn As Weapon) As Double
        If wpn.qualities.Exists(Function(x) x.ToLower = "slow") Then
            RangedAttackMoveMod = 1
        Else
            'RangedAttackMoveMod = 1 + (move / (avgtroop.move * 2))
            RangedAttackMoveMod = 1 + (move / avgtroop.move)
        End If
    End Function
    Public Function PriceAttackArray(atkarray As AttackArrayType) As Double
        PriceAttackArray = 0
        For Each atk As AttackType In atkarray.attacks
            PriceAttackArray += atk.pointValue
        Next
    End Function
    Public Function PriceOffense(totOffense As OffenseType) As Double()
        'Returns an array of two doubles.  (0) is melee, (1) is ranged

        Dim meleeCost As Double = 0
        Dim rangedCost As Double = 0
        'Dim comboCost As Double = 0
        Dim meleeAttackValues As List(Of Double) = New List(Of Double)
        Dim rangedAttackValues As List(Of Double) = New List(Of Double)
        'Dim comboAttackValues As List(Of Double) = New List(Of Double)
        Dim meleeMod As Double = 1
        Dim rangedMod As Double = 1

        For Each qual In frmMain.selectedTroopQualities
            For Each moditem In qual.modifiers
                If moditem.name.ToLower = "rangedcostmultiplier" Then
                    rangedMod = rangedMod * moditem.value
                ElseIf moditem.name.ToLower = "meleecostmultiplier" Then
                    meleeMod = meleeMod * moditem.value
                End If
            Next
        Next


        For Each atkarray As AttackArrayType In totOffense.attackArrays
            If atkarray.type.ToLower = "melee" Then
                meleeAttackValues.Add(atkarray.pointValue)
            ElseIf atkarray.type.ToLower = "ranged" Then
                rangedAttackValues.Add(atkarray.pointValue)
                'ElseIf atkarray.type.ToLower = "both" Then
                '    comboAttackValues.Add(atkarray.pointValue)
            End If
        Next

        If meleeAttackValues.Count > 0 Then
            For Each PV As Double In meleeAttackValues
                meleeCost += PV / 2
            Next
            meleeCost += meleeAttackValues.Max / 2
        End If

        If rangedAttackValues.Count > 0 Then
            For Each PV As Double In rangedAttackValues
                rangedCost += PV / 2
            Next
            rangedCost += rangedAttackValues.Max / 2
        End If

        'If comboAttackValues.Count > 0 Then
        '    For Each PV As Double In comboAttackValues
        '        comboCost += PV / 2
        '    Next
        '    comboCost += comboAttackValues.Max / 2
        'End If

        meleeCost = meleeCost * meleeMod
        rangedCost = rangedCost * rangedMod
        'comboCost = comboCost * rangedMod * meleeMod

        'PriceOffense = meleeCost + rangedCost + (comboCost * 1.5)
        PriceOffense = {meleeCost, rangedCost}
    End Function

    'Public Function PriceDefense(meleeSkill As Integer, shieldTF As Boolean, armor As Integer, wounds As Integer, wpn As Weapon, offhandBonus As Integer, troopQualities As List(Of QualityType)) As Double
    'Public Function PriceDefense(meleeSkill As Integer, shieldTF As Boolean, armor As Integer, wounds As Integer, wpnParry As Integer, offhandBonus As Integer, size As String, troopQualities As List(Of QualityType)) As Double()
    Public Function PriceDefense(meleeSkill As Integer, shieldTF As Boolean, wndFirst As Integer, wndMore As Integer, wounds As Integer, wpnParry As Integer, offhandBonus As Integer, size As String, troopQualities As List(Of QualityType)) As Double()
        Dim parry, dodge As Integer
        Dim wpnDefBonus As Integer = wpnParry
        Dim tmod As ModifierType
        Dim shield As Integer
        Dim parrysizemod As Integer
        Dim armorBonus As Integer = 0

        If shieldTF Then
            shield = 1
        Else
            shield = 0
        End If

        parrysizemod = 0
        If size = "Large" Or size = "Huge" Then
            parrysizemod = -1
        End If

        parry = 5 + parrysizemod + meleeSkill + wpnDefBonus + offhandBonus + shield
        dodge = 6 + shield
        Dim w As Integer = wounds

        For Each qual As QualityType In troopQualities
            tmod = qual.modifiers.Find(Function(x) x.name.ToLower = "rangeddefense")
            dodge += tmod.value

            tmod = qual.modifiers.Find(Function(x) x.name.ToLower = "meleedefense")
            parry += tmod.value

            'tmod = qual.modifiers.Find(Function(x) x.name.ToLower = "armor")
            'armorBonus += tmod.value

            If qual.name.ToLower = "dispersed" Then
                w = 1
            End If
        Next
        'wndFirst += armorBonus

        'Dim hits As Integer = HitsPerAttack(avgtroop.tohit, parry)
        'Dim crits As Integer = HitsPerAttack(avgtroop.tohit, parry + 5)
        'Dim dblcrits As Integer = HitsPerAttack(avgtroop.tohit, parry + 10)
        Dim hits As Integer = 11 - parry + avgtroop.tohit

        Dim dmgbase As Double = WoundsPerHit(avgtroop.dmgDie, avgtroop.dmgMod, 0, wndFirst, wndMore, w)
        Dim dmgplus1 As Double = WoundsPerHit(avgtroop.dmgDie, avgtroop.dmgMod + 1, 0, wndFirst, wndMore, w)
        Dim dmgplus2 As Double = WoundsPerHit(avgtroop.dmgDie, avgtroop.dmgMod + 2, 0, wndFirst, wndMore, w)
        Dim dmgavg As Double = ((dmgbase * 4) + (dmgplus1 * 2) + (dmgplus2)) / 7

        Dim wnds As Double
        'wnds += hits * dmgbase
        wnds += hits * dmgavg

        Dim avgwndsMelee As Double = wnds / 10

        hits = 11 - dodge + avgtroop.tohit

        wnds = 0
        'wnds += hits * dmgbase
        wnds += hits * dmgavg

        Dim avgwndsRanged As Double = wnds / 10

        Dim avgwndsPerTurn = (avgwndsMelee + avgwndsRanged) / 2

        PriceDefense = {wounds / avgwndsPerTurn, avgwndsMelee, avgwndsRanged}

        'frmMain.lblArmor.Text = armor.ToString

        frmMain.lblToWound.Text = wndFirst.ToString + " / +" + wndMore.ToString
        'frmMain.lblMeleeDef.Text = (parry - shield - parrysizemod).ToString
        frmMain.lblMeleeDef.Text = (parry - parrysizemod).ToString
        'frmMain.lblRangedDef.Text = (dodge - shield).ToString
        frmMain.lblRangedDef.Text = dodge.ToString

        If shieldTF = True Then
            'frmMain.lblMeleeDef.Text += "+"
            'frmMain.lblRangedDef.Text += "+"
        End If
    End Function

    Public Function PriceMagic(casterLevel As Double, costPerLevel As Double, spellSourcesKnown As Integer, singlespell As Boolean) As Double
        PriceMagic = 0
        If Not singlespell Then
            PriceMagic = (casterLevel * costPerLevel * 2) ' The 2 comes from two other spells, each halved as secondary attacks.
        Else
            PriceMagic = (casterLevel * costPerLevel)
        End If


        'Change cost based on ranged modifier
        Dim rangedmod As Double
        rangedmod = CInt(frmMain.txtRangedSkill.Text) * 0.1
        rangedmod = rangedmod + 1
        PriceMagic = PriceMagic * rangedmod

        'Change cost based on move
        Dim RangedAttackMoveMod As Double
        RangedAttackMoveMod = (CInt(frmMain.txtMove.Text) / avgtroop.move)
        PriceMagic = PriceMagic * RangedAttackMoveMod


        'PriceMagic = (casterLevel * costPerLevel)
        'PriceMagic += ((spellSourcesKnown - 1) * (costPerLevel / 2))
        'If Not singlespell And casterLevel > 1 Then
        '    PriceMagic += ((casterLevel - 1) * (costPerLevel / 2))
        '    Dim i As Integer = casterLevel - 2
        '    While i > 0
        '    PriceMagic += i * costPerLevel / 4
        '    i -= 1
        '    End While
        'End If

    End Function
End Module
