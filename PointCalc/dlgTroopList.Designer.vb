﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgTroopList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFileName = New System.Windows.Forms.Label()
        Me.lstTroops = New System.Windows.Forms.ListView()
        Me.TroopName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PointValue = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblFileName
        '
        Me.lblFileName.CausesValidation = False
        Me.lblFileName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(37, 33)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(299, 21)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lstTroops
        '
        Me.lstTroops.AllowColumnReorder = True
        Me.lstTroops.AutoArrange = False
        Me.lstTroops.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.TroopName, Me.PointValue})
        Me.lstTroops.FullRowSelect = True
        Me.lstTroops.Location = New System.Drawing.Point(40, 72)
        Me.lstTroops.MultiSelect = False
        Me.lstTroops.Name = "lstTroops"
        Me.lstTroops.Size = New System.Drawing.Size(296, 250)
        Me.lstTroops.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstTroops.TabIndex = 4
        Me.lstTroops.UseCompatibleStateImageBehavior = False
        Me.lstTroops.View = System.Windows.Forms.View.Details
        '
        'TroopName
        '
        Me.TroopName.Text = "Name"
        Me.TroopName.Width = 196
        '
        'PointValue
        '
        Me.PointValue.Text = "Point Value"
        Me.PointValue.Width = 94
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(362, 72)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(71, 37)
        Me.btnLoad.TabIndex = 5
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(362, 285)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(71, 37)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "Delete Selected"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(201, 359)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(71, 37)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Close"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'dlgTroopList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(473, 425)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.lstTroops)
        Me.Controls.Add(Me.lblFileName)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgTroopList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Troop List"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents lstTroops As System.Windows.Forms.ListView
    Friend WithEvents TroopName As System.Windows.Forms.ColumnHeader
    Friend WithEvents PointValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button

End Class
