﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgAdvancedAttacks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.lstAttackArrays = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lstAttacks = New System.Windows.Forms.ListBox()
        Me.btnAttackArrayAdd = New System.Windows.Forms.Button()
        Me.btnDeleteAttackArray = New System.Windows.Forms.Button()
        Me.btnRenameAttackArray = New System.Windows.Forms.Button()
        Me.btnAtkArrayMoveUp = New System.Windows.Forms.Button()
        Me.btnAtkArrayMoveDn = New System.Windows.Forms.Button()
        Me.btnAttackAdd = New System.Windows.Forms.Button()
        Me.btnDeleteAttack = New System.Windows.Forms.Button()
        Me.btnAttackRename = New System.Windows.Forms.Button()
        Me.btnAttackMoveUp = New System.Windows.Forms.Button()
        Me.btnAttackMoveDn = New System.Windows.Forms.Button()
        Me.txtCustAttack = New System.Windows.Forms.TextBox()
        Me.btnEditAttack = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtParryMod = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(261, 536)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Save"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'lstAttackArrays
        '
        Me.lstAttackArrays.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAttackArrays.FormattingEnabled = True
        Me.lstAttackArrays.ItemHeight = 16
        Me.lstAttackArrays.Location = New System.Drawing.Point(28, 58)
        Me.lstAttackArrays.Name = "lstAttackArrays"
        Me.lstAttackArrays.Size = New System.Drawing.Size(191, 212)
        Me.lstAttackArrays.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(194, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Attack Sets"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(352, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(194, 24)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Attacks"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lstAttacks
        '
        Me.lstAttacks.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstAttacks.FormattingEnabled = True
        Me.lstAttacks.ItemHeight = 16
        Me.lstAttacks.Location = New System.Drawing.Point(355, 58)
        Me.lstAttacks.Name = "lstAttacks"
        Me.lstAttacks.Size = New System.Drawing.Size(191, 212)
        Me.lstAttacks.TabIndex = 3
        '
        'btnAttackArrayAdd
        '
        Me.btnAttackArrayAdd.Location = New System.Drawing.Point(28, 276)
        Me.btnAttackArrayAdd.Name = "btnAttackArrayAdd"
        Me.btnAttackArrayAdd.Size = New System.Drawing.Size(55, 32)
        Me.btnAttackArrayAdd.TabIndex = 5
        Me.btnAttackArrayAdd.Text = "New"
        Me.btnAttackArrayAdd.UseVisualStyleBackColor = True
        '
        'btnDeleteAttackArray
        '
        Me.btnDeleteAttackArray.Location = New System.Drawing.Point(164, 276)
        Me.btnDeleteAttackArray.Name = "btnDeleteAttackArray"
        Me.btnDeleteAttackArray.Size = New System.Drawing.Size(55, 32)
        Me.btnDeleteAttackArray.TabIndex = 6
        Me.btnDeleteAttackArray.Text = "Delete"
        Me.btnDeleteAttackArray.UseVisualStyleBackColor = True
        '
        'btnRenameAttackArray
        '
        Me.btnRenameAttackArray.Location = New System.Drawing.Point(96, 276)
        Me.btnRenameAttackArray.Name = "btnRenameAttackArray"
        Me.btnRenameAttackArray.Size = New System.Drawing.Size(55, 32)
        Me.btnRenameAttackArray.TabIndex = 7
        Me.btnRenameAttackArray.Text = "Rename"
        Me.btnRenameAttackArray.UseVisualStyleBackColor = True
        '
        'btnAtkArrayMoveUp
        '
        Me.btnAtkArrayMoveUp.Location = New System.Drawing.Point(225, 97)
        Me.btnAtkArrayMoveUp.Name = "btnAtkArrayMoveUp"
        Me.btnAtkArrayMoveUp.Size = New System.Drawing.Size(55, 25)
        Me.btnAtkArrayMoveUp.TabIndex = 8
        Me.btnAtkArrayMoveUp.Text = "Up"
        Me.btnAtkArrayMoveUp.UseVisualStyleBackColor = True
        '
        'btnAtkArrayMoveDn
        '
        Me.btnAtkArrayMoveDn.Location = New System.Drawing.Point(225, 128)
        Me.btnAtkArrayMoveDn.Name = "btnAtkArrayMoveDn"
        Me.btnAtkArrayMoveDn.Size = New System.Drawing.Size(55, 25)
        Me.btnAtkArrayMoveDn.TabIndex = 9
        Me.btnAtkArrayMoveDn.Text = "Down"
        Me.btnAtkArrayMoveDn.UseVisualStyleBackColor = True
        '
        'btnAttackAdd
        '
        Me.btnAttackAdd.Location = New System.Drawing.Point(354, 276)
        Me.btnAttackAdd.Name = "btnAttackAdd"
        Me.btnAttackAdd.Size = New System.Drawing.Size(55, 32)
        Me.btnAttackAdd.TabIndex = 5
        Me.btnAttackAdd.Text = "New"
        Me.btnAttackAdd.UseVisualStyleBackColor = True
        '
        'btnDeleteAttack
        '
        Me.btnDeleteAttack.Location = New System.Drawing.Point(490, 276)
        Me.btnDeleteAttack.Name = "btnDeleteAttack"
        Me.btnDeleteAttack.Size = New System.Drawing.Size(55, 32)
        Me.btnDeleteAttack.TabIndex = 6
        Me.btnDeleteAttack.Text = "Delete"
        Me.btnDeleteAttack.UseVisualStyleBackColor = True
        '
        'btnAttackRename
        '
        Me.btnAttackRename.Location = New System.Drawing.Point(422, 276)
        Me.btnAttackRename.Name = "btnAttackRename"
        Me.btnAttackRename.Size = New System.Drawing.Size(55, 32)
        Me.btnAttackRename.TabIndex = 7
        Me.btnAttackRename.Text = "Rename"
        Me.btnAttackRename.UseVisualStyleBackColor = True
        '
        'btnAttackMoveUp
        '
        Me.btnAttackMoveUp.Location = New System.Drawing.Point(552, 97)
        Me.btnAttackMoveUp.Name = "btnAttackMoveUp"
        Me.btnAttackMoveUp.Size = New System.Drawing.Size(55, 25)
        Me.btnAttackMoveUp.TabIndex = 8
        Me.btnAttackMoveUp.Text = "Up"
        Me.btnAttackMoveUp.UseVisualStyleBackColor = True
        '
        'btnAttackMoveDn
        '
        Me.btnAttackMoveDn.Location = New System.Drawing.Point(552, 128)
        Me.btnAttackMoveDn.Name = "btnAttackMoveDn"
        Me.btnAttackMoveDn.Size = New System.Drawing.Size(55, 25)
        Me.btnAttackMoveDn.TabIndex = 9
        Me.btnAttackMoveDn.Text = "Down"
        Me.btnAttackMoveDn.UseVisualStyleBackColor = True
        '
        'txtCustAttack
        '
        Me.txtCustAttack.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustAttack.Location = New System.Drawing.Point(28, 416)
        Me.txtCustAttack.Multiline = True
        Me.txtCustAttack.Name = "txtCustAttack"
        Me.txtCustAttack.ReadOnly = True
        Me.txtCustAttack.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtCustAttack.Size = New System.Drawing.Size(517, 114)
        Me.txtCustAttack.TabIndex = 10
        '
        'btnEditAttack
        '
        Me.btnEditAttack.Location = New System.Drawing.Point(552, 197)
        Me.btnEditAttack.Name = "btnEditAttack"
        Me.btnEditAttack.Size = New System.Drawing.Size(55, 32)
        Me.btnEditAttack.TabIndex = 5
        Me.btnEditAttack.Text = "Edit"
        Me.btnEditAttack.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(26, 337)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 15)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Parry Modifier:"
        '
        'txtParryMod
        '
        Me.txtParryMod.Location = New System.Drawing.Point(118, 336)
        Me.txtParryMod.Name = "txtParryMod"
        Me.txtParryMod.Size = New System.Drawing.Size(52, 20)
        Me.txtParryMod.TabIndex = 12
        '
        'dlgAdvancedAttacks
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(674, 577)
        Me.Controls.Add(Me.txtParryMod)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCustAttack)
        Me.Controls.Add(Me.btnAttackMoveDn)
        Me.Controls.Add(Me.btnAtkArrayMoveDn)
        Me.Controls.Add(Me.btnAttackMoveUp)
        Me.Controls.Add(Me.btnAtkArrayMoveUp)
        Me.Controls.Add(Me.btnAttackRename)
        Me.Controls.Add(Me.btnDeleteAttack)
        Me.Controls.Add(Me.btnRenameAttackArray)
        Me.Controls.Add(Me.btnEditAttack)
        Me.Controls.Add(Me.btnAttackAdd)
        Me.Controls.Add(Me.btnDeleteAttackArray)
        Me.Controls.Add(Me.btnAttackArrayAdd)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lstAttacks)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstAttackArrays)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgAdvancedAttacks"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Custom Attacks"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents lstAttackArrays As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lstAttacks As System.Windows.Forms.ListBox
    Friend WithEvents btnAttackArrayAdd As System.Windows.Forms.Button
    Friend WithEvents btnDeleteAttackArray As System.Windows.Forms.Button
    Friend WithEvents btnRenameAttackArray As System.Windows.Forms.Button
    Friend WithEvents btnAtkArrayMoveUp As System.Windows.Forms.Button
    Friend WithEvents btnAtkArrayMoveDn As System.Windows.Forms.Button
    Friend WithEvents btnAttackAdd As System.Windows.Forms.Button
    Friend WithEvents btnDeleteAttack As System.Windows.Forms.Button
    Friend WithEvents btnAttackRename As System.Windows.Forms.Button
    Friend WithEvents btnAttackMoveUp As System.Windows.Forms.Button
    Friend WithEvents btnAttackMoveDn As System.Windows.Forms.Button
    Friend WithEvents txtCustAttack As System.Windows.Forms.TextBox
    Friend WithEvents btnEditAttack As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtParryMod As System.Windows.Forms.TextBox

End Class
