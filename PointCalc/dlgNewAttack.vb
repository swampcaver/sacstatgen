﻿Imports System.Windows.Forms

Public Class dlgNewAttack

    Public Property propNewAttack As AttackType
        Get
            Return newAtk
        End Get
        Set(value As AttackType)

        End Set
    End Property
    Public Property Qualities As List(Of QualityType)
        Get
            Return selectedQualities
        End Get
        Set(value As List(Of QualityType))
            selectedQualities = value
            For Each qual As QualityType In selectedQualities
                selectedStrQualities.Add(qual.name)
            Next
        End Set
    End Property

    Dim newAtk As AttackType
    Dim selectedQualities As List(Of QualityType) = New List(Of QualityType)
    Dim selectedStrQualities As List(Of String) = New List(Of String)


    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If txtName.Text = "" Then
            MessageBox.Show("Name cannot be blank")
            txtName.Focus()
            Exit Sub
        End If

        If txtDmgMod.Text = "" Or Not IsNumeric(txtDmgMod.Text) Then
            MessageBox.Show("Damage modifier must be an integer")
            txtDmgMod.Focus()
            Exit Sub
        End If

        If txtNumAttacks.Text = "" Or Not IsNumeric(txtNumAttacks.Text) Then
            MessageBox.Show("Number of attacks must be a positive integer")
            txtNumAttacks.Focus()
            Exit Sub
        End If

        If CInt(txtNumAttacks.Text) < 1 Then
            MessageBox.Show("Number of attacks must be a positive integer")
            txtNumAttacks.Focus()
            Exit Sub
        End If

        If cbRanged.Checked = True Then

            If txtRange.Text = "" Or Not IsNumeric(txtRange.Text) Then
                MessageBox.Show("Range must be a positive integer")
                txtRange.Focus()
                Exit Sub
            End If

            If CInt(txtRange.Text) < 1 Then
                MessageBox.Show("Range must be a positive integer")
                txtRange.Focus()
                Exit Sub
            End If
        End If

        Dim wpn As Weapon = New Weapon
        wpn.dmgDie = CInt(cbDmgDie.SelectedItem)
        wpn.qualities = selectedStrQualities
        If IsNothing(wpn.qualities) Then
            wpn.qualities = New List(Of String)
        End If
        If cbRanged.Checked = True Then
            Dim newAtkMedRange As AttackType = New AttackType
            Dim newAtkLongRange As AttackType = New AttackType
            Dim rangedavgtroop As basicOpponent = avgtroop
            rangedavgtroop.parry = 6
            newAtkLongRange = GenerateMeleeAttack(CInt(txtToHit.Text) - 2, CInt(txtDmgMod.Text), CInt(txtNumAttacks.Text), selectedQualities, wpn, rangedavgtroop)
            newAtkMedRange = GenerateMeleeAttack(CInt(txtToHit.Text) - 1, CInt(txtDmgMod.Text), CInt(txtNumAttacks.Text), selectedQualities, wpn, rangedavgtroop)
            newAtk = GenerateMeleeAttack(CInt(txtToHit.Text), CInt(txtDmgMod.Text), CInt(txtNumAttacks.Text), selectedQualities, wpn, rangedavgtroop)
            If wpn.qualities.Exists(Function(x) x.ToLower = "minimum range") Then
                newAtk.pointValue = newAtk.pointValue / 2
            End If
            'Account for range
            newAtk.pointValue = (newAtk.pointValue + newAtkMedRange.pointValue + newAtkLongRange.pointValue)
            newAtk.pointValue = newAtk.pointValue * CInt(txtRange.Text)
            newAtk.pointValue = newAtk.pointValue / (avgtroop.move * 2)
            newAtk.pointValue = newAtk.pointValue * RangedAttackMoveMod(CInt(frmMain.txtMove.Text), wpn)
            'assume the attack will go against a high value target
            newAtk.pointValue = newAtk.pointValue * 3

            newAtk.type = "ranged"
            newAtk.range = CInt(txtRange.Text)
        Else
            newAtk = GenerateMeleeAttack(CInt(txtToHit.Text), CInt(txtDmgMod.Text), CInt(txtNumAttacks.Text), selectedQualities, wpn, avgtroop)
            newAtk.type = "melee"
        End If

        newAtk.name = txtName.Text

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub


    Private Sub cbRanged_CheckedChanged(sender As Object, e As EventArgs) Handles cbRanged.CheckedChanged

        If Me.IsHandleCreated = False Then Exit Sub

        Dim cb As CheckBox = sender
        If cb.Checked = True Then
            txtRange.Enabled = True
            If txtRange.Text = "" Then txtRange.Text = "6"
            txtToHit.Text = frmMain.txtRangedSkill.Text
        Else
            txtRange.Enabled = False
            txtToHit.Text = frmMain.txtMeleeSkill.Text
        End If
    End Sub

    Private Sub dlgNewAttack_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'cbRanged.Checked = False
        If txtName.Text = "" Then
            txtName.Text = "New Attack"
        End If
        If txtToHit.Text = "" Then
            txtToHit.Text = frmMain.txtMeleeSkill.Text
        End If
        If cbDmgDie.SelectedItem = "" Then
            cbDmgDie.SelectedItem = "6"
        End If
        If txtDmgMod.Text = "" Then
            txtDmgMod.Text = frmMain.txtStrength.Text
        End If
        If txtNumAttacks.Text = "" Then
            txtNumAttacks.Text = "1"
        End If
        If IsNothing(selectedQualities) Then
            selectedQualities = New List(Of QualityType)
        End If

    End Sub


    Private Sub btnSelectQualities_Click(sender As Object, e As EventArgs) Handles btnSelectQualities.Click
        'Dim selectedQualities As List(Of String)
        Dim cancelled As Boolean = False
        Dim qual As QualityType = New QualityType
        'selectedQualities = New List(Of String)
        Dim newSelectedQualities As List(Of String) = New List(Of String)

        newSelectedQualities = frmMain.LaunchQualitySelectDlg(frmMain.wpnQualities, selectedQualities, cancelled)


        If cancelled Then
            Exit Sub
        End If

        txtQualities.Text = ""
        selectedQualities.Clear()
        selectedStrQualities = newSelectedQualities

        For Each qualstr In newSelectedQualities
            qual = frmMain.wpnQualities.Find(Function(x) x.name.ToLower = qualstr.ToLower)
            selectedQualities.Add(qual)
            txtQualities.Text += qualstr
            If newSelectedQualities.Last <> qualstr Then
                txtQualities.Text += "; "
            End If
        Next

    End Sub
End Class
