﻿Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Module SaCXML
    Public Function XMLFileVersion(filename As String) As Double
        If Not (IO.File.Exists(filename)) Then
            XMLFileVersion = 0
            Exit Function
        End If

        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(filename)
        Dim xmlroot As XmlNode = xmlDoc.DocumentElement
        Dim versionNode As XmlNode = xmlroot.SelectSingleNode("version")
        XMLFileVersion = CDbl(versionNode.InnerText)

    End Function

    Public Function TroopToXML() As XmlDocument
        Dim troop As TroopType
        With troop
            .name = frmMain.txtTroopName.Text
            .pointValue = frmMain.lblPointValue.Text
            .race = frmMain.cbRace.SelectedItem.ToString
            .template = frmMain.cbTemplate.SelectedItem.ToString
            .move = frmMain.txtMove.Text
            .str = frmMain.txtStrength.Text
            .meleeSkill = frmMain.txtMeleeSkill.Text
            .rangedSkill = frmMain.txtRangedSkill.Text
            .armor = frmMain.cbArmor.SelectedItem.ToString
            .armorCustom = frmMain.chkArmorName.Checked
            .armorCustomName = frmMain.txtArmorName.Text
            .toWoundFirst = frmMain.toWoundFirst
            .toWoundMore = frmMain.toWoundMore
            .shield = frmMain.chkShield.Checked
            .wounds = frmMain.txtWounds.Text
            .size = frmMain.cbSize.SelectedItem.ToString
            .qualitiesString = frmMain.txtTroopQualities.Text
            .qualitiesList = frmMain.selectedTroopQualities
            .weaponPrimary = frmMain.cbMeleeWpnsPri.SelectedItem.ToString
            .weaponOffhand = frmMain.cbMeleeWpnsOff.SelectedItem.ToString
            .weaponRanged = frmMain.cbRangedWeapons.SelectedItem.ToString
            If frmMain.radOffHandBonus.Checked Then
                .offhandOptions = 1
            ElseIf frmMain.radOffHandSec.Checked Then
                .offhandOptions = 2
            Else
                .offhandOptions = 3
            End If
            .customAttacks = frmMain.CustomAttacks
            .customParryMod = frmMain.CustomAttackParryMod
            .DefenseMelee = frmMain.lblMeleeDef.Text
            .DefenseRanged = frmMain.lblRangedDef.Text
            '.DefenseArmor = frmMain.lblArmorOLD.Text
            .spellcaster = frmMain.chkSpellCaster.Checked
            .spellLevel = CDbl(frmMain.cbCasterLevel.SelectedItem.ToString)
            .spellSourcesKnown = CDbl(frmMain.cbSpellSources.SelectedItem.ToString)
            .spellSingle = frmMain.chkSingleSpell.Checked
            .factions = frmMain.selectedFactions
            .factionString = frmMain.txtFactions.Text
            .offense = frmMain.offense
            .healCost = frmMain.lblHealCost.Text
        End With
        Dim xmlDoc As XmlDocument = New XmlDocument
        Dim xmlSerial As XmlSerializer = New XmlSerializer(troop.GetType)
        Dim xmlStream As MemoryStream = New MemoryStream()

        xmlSerial.Serialize(xmlStream, troop)
        xmlStream.Position = 0
        xmlDoc.Load(xmlStream)

        TroopToXML = xmlDoc

    End Function

    Public Function XMLToTroop(xmlString As String) As TroopType
        XMLToTroop = New TroopType
        Dim xmlSerial As XmlSerializer = New XmlSerializer(XMLToTroop.GetType)
        Dim xmlStream As StringReader = New StringReader(xmlString)

        XMLToTroop = xmlSerial.Deserialize(xmlStream)

    End Function

    Public Function AddXMLFormatting(xmlstring As String) As String
        AddXMLFormatting = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & "?><TroopType xmlns:xsi=" & Chr(34) & "http://www.w3.org/2001/XMLSchema-instance" & Chr(34) & " xmlns:xsd=" & Chr(34) & "http://www.w3.org/2001/XMLSchema" & Chr(34) & "> "
        AddXMLFormatting += xmlstring
        AddXMLFormatting += "</TroopType>"
    End Function
End Module
