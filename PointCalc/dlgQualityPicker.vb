﻿Imports System.Windows.Forms

Public Class dlgQualityPicker

    Public Property SelectedQualities As ListBox.ObjectCollection
        Get
            Return lstSelectedQualities.Items
        End Get
        Set(value As ListBox.ObjectCollection)

        End Set
    End Property

    Private Sub btnSelect_Click(sender As Object, e As EventArgs) Handles btnSelect.Click, lstAllQualities.DoubleClick
        Dim selectedItems As List(Of String) = New List(Of String)
        For Each qual As String In lstAllQualities.SelectedItems
            lstSelectedQualities.Items.Add(qual)
            selectedItems.Add(qual)
        Next
        For Each qual As String In selectedItems
            lstAllQualities.Items.Remove(qual)
        Next
    End Sub

    Private Sub btnDeSelect_Click(sender As Object, e As EventArgs) Handles btnDeSelect.Click, lstSelectedQualities.DoubleClick
        Dim selectedItems As List(Of String) = New List(Of String)
        For Each qual As String In lstSelectedQualities.SelectedItems
            lstAllQualities.Items.Add(qual)
            selectedItems.Add(qual)
        Next
        For Each qual As String In selectedItems
            lstSelectedQualities.Items.Remove(qual)
        Next
    End Sub

    Private Sub OK_Button_Click(sender As Object, e As EventArgs) Handles OK_Button.Click

    End Sub
End Class
