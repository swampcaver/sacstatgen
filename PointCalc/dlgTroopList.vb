﻿Imports System.Windows.Forms
Imports System.Xml

Public Class dlgTroopList
    Dim GlobalSource As Boolean = False
    Dim xmlsrcdoc As XmlDocument
    Dim xmlsrcroot As XmlElement

    Public Property UseGlobalList As Boolean
        Get
            Return GlobalSource
        End Get
        Set(value As Boolean)
            GlobalSource = value
        End Set
    End Property
    
    Private Sub dlgTroopList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        xmlsrcdoc = New XmlDocument
        If GlobalSource = True Then
            xmlsrcdoc.Load(frmMain.GlobalTroopListFile)
            xmlsrcroot = xmlsrcdoc.DocumentElement
            btnDelete.Enabled = False
        Else
            xmlsrcdoc = frmMain.UserTroopXMLDoc
            xmlsrcroot = frmMain.UserTroopXMLroot
        End If

        LoadTroopList()

    End Sub
    Private Sub LoadTroopList()

        lstTroops.Items.Clear()

        Dim name, pv As String
        Dim nodelist As XmlNodeList = xmlsrcroot.SelectNodes("//troop_list/troop")
        For Each node As XmlNode In nodelist
            'MessageBox.Show(node.InnerText)
            name = node.SelectSingleNode("name").InnerText
            pv = node.SelectSingleNode("pointValue").InnerText
            lstTroops.Items.Add(New ListViewItem({name, pv}))
        Next

        lstTroops.Sort()
        Dim findLast As ListViewItem
        If GlobalSource = True Then
            findLast = lstTroops.FindItemWithText(frmMain.lastTroopFromGlobal)
        Else
            findLast = lstTroops.FindItemWithText(frmMain.lastTroopFromLocal)
        End If

        If findLast IsNot Nothing Then
            lstTroops.EnsureVisible(findLast.Index)
        End If

    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click, lstTroops.DoubleClick

        If lstTroops.SelectedItems.Count = 0 Then Exit Sub

        Dim working As New dlgWorking
        working.Label.Text = "Loading troop settings..."
        working.Show()
        working.Refresh()

        Dim selectedname As String = lstTroops.SelectedItems(0).Text
        Dim troops As XmlNodeList = xmlsrcroot.SelectNodes("//troop_list/troop")
        Dim troopnode As XmlNode
        Dim found As Boolean = False
        For Each troopnode In troops
            Name = troopnode.SelectSingleNode("name").InnerText
            If Name.ToLower = selectedname.ToLower Then
                found = True
                Exit For
            End If
        Next

        If found = False Then
            working.Close()
            MessageBox.Show("An error occurred while loading " & selectedname)
            Exit Sub
        End If

        LoadTroop(troopnode)
        If GlobalSource = True Then
            frmMain.lastTroopFromGlobal = selectedname
        Else
            frmMain.lastTroopFromLocal = selectedname
        End If


        working.Close()
        Me.Close()

    End Sub
    Public Sub LoadTroop(trpNode As XmlNode)
        Dim troopstr As String = AddXMLFormatting(trpNode.InnerXml)
        Dim troop As TroopType = XMLToTroop(AddXMLFormatting(trpNode.InnerXml))

        frmMain.Initializing = True
        With troop
            frmMain.txtTroopName.Text = .name
            frmMain.lblPointValue.Text = .pointValue
            frmMain.cbRace.SelectedItem = .race
            frmMain.cbTemplate.SelectedItem = .template
            frmMain.txtMove.Text = .move
            frmMain.txtStrength.Text = .str
            frmMain.txtMeleeSkill.Text = .meleeSkill
            frmMain.txtRangedSkill.Text = .rangedSkill
            frmMain.cbArmor.SelectedItem = .armor
            frmMain.chkArmorName.Checked = .armorCustom
            frmMain.txtArmorName.Text = .armorCustomName
            frmMain.chkShield.Checked = .shield
            frmMain.txtWounds.Text = .wounds
            frmMain.cbSize.SelectedItem = .size
            frmMain.txtTroopQualities.Text = .qualitiesString
            frmMain.selectedTroopQualities = .qualitiesList
            frmMain.cbMeleeWpnsPri.SelectedItem = .weaponPrimary
            frmMain.cbMeleeWpnsOff.SelectedItem = .weaponOffhand
            frmMain.cbRangedWeapons.SelectedItem = .weaponRanged
            If .offhandOptions = 1 Then
                frmMain.radOffHandBonus.Checked = True
            ElseIf .offhandOptions = 2 Then
                frmMain.radOffHandSec.Checked = True
            Else
                frmMain.radOffhandBoth.Checked = True
            End If
            frmMain.CustomAttacks = .customAttacks
            frmMain.CustomAttackParryMod = .customParryMod
            frmMain.offense = .offense
            frmMain.chkSpellCaster.Checked = .spellcaster
            If .spellcaster Then
                frmMain.cbCasterLevel.SelectedItem = CInt(.spellLevel)
                frmMain.cbSpellSources.SelectedItem = CInt(.spellSourcesKnown)
                frmMain.chkSingleSpell.Checked = .spellSingle
            End If
            frmMain.selectedFactions = .factions
            If .factionString = "" Then
                frmMain.txtFactions.Text = ""
                For Each fac In frmMain.selectedFactions
                    frmMain.txtFactions.Text += fac
                    If frmMain.selectedFactions.Last <> fac Then
                        frmMain.txtFactions.Text += "; "
                    End If
                Next
            Else
                frmMain.txtFactions.Text = .factionString
            End If
        End With
        frmMain.Initializing = False

        If frmMain.CustomAttacks = False Then
            frmMain.RevertFormToBasicAttacks()
        Else
            frmMain.ConvertFormToCustomAttacks()
        End If

        frmMain.UpdateQualityModifiers()
        frmMain.RecalculateOffense()
        frmMain.RefreshCalculations()
    End Sub
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If lstTroops.SelectedItems.Count = 0 Then Exit Sub
        Dim oldnode As XmlNode

        Dim working As New dlgWorking
        working.Label.Text = "Deleting selected troop..."
        working.Show()
        working.Refresh()

        Dim selectedname As String = lstTroops.SelectedItems(0).Text
        Dim troops As XmlNodeList = xmlsrcroot.SelectNodes("//troop_list/troop")
        Dim troopnode As XmlNode
        Dim found As Boolean = False
        For Each troopnode In troops
            Name = troopnode.SelectSingleNode("name").InnerText
            If Name.ToLower = selectedname.ToLower Then
                found = True
                oldnode = troopnode
                Exit For
            End If
        Next

        If found = False Then
            working.Close()
            MessageBox.Show("An error occurred while deleting " & selectedname)
            Exit Sub
        End If

        '        Dim troopstr As String = AddXMLFormatting(troopnode.InnerXml)
        '        Dim troop As TroopType = XMLToTroop(AddXMLFormatting(troopnode.InnerXml))
        xmlsrcroot.RemoveChild(oldnode)

        If UseGlobalList = True Then
            xmlsrcdoc.Save(frmMain.GlobalTroopListFile)
        Else
            xmlsrcdoc.Save(frmMain.UserTroopFileName)
        End If

        LoadTroopList()

        working.Close()
    End Sub

   
End Class